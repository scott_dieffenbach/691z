﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ElectionSeason.Utilities;

namespace ElectionSeason
{
    public partial class LeagueConfig : System.Web.UI.Page
    {
        string leagueName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (leagueName == "")
            {
                leagueName = Request.QueryString["ln"].ToString();
            }
            else
            {
                string strException = "League not found. Please try again.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + strException + "');  window.location='" + Request.ApplicationPath + "leagueconfig.aspx';", true);
            }
        }
        protected override void OnInit(EventArgs e)
        {
            this.ddlScreens.SelectedIndexChanged += new EventHandler(ddlScreens_SelectedIndexChanged);
            this.ddlScreens.AutoPostBack = true;
            SetDropDownList();
            base.OnInit(e);
        }
        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }
        protected void ddlScreens_SelectedIndexChanged(object sender, EventArgs e)
        {
                int selectedValue = ddlScreens.SelectedIndex;
                int leagueId = Convert.ToInt16(Request.QueryString["lid"]);
                int leagueSize = ESDBUtility.GetLeagueSize(leagueId);

                //string SessionID = Session["IdNumber"].ToString();
                //string datatablecache = ("leagueconfig" + SessionID);
                //DataTable dt = ESDBUtility.GetLeagueInvities(leagueId);
                //Cache.Insert(datatablecache, dt);
                
                string leagueName = Request.QueryString["ln"];

                if (selectedValue == 1)
                {
                    Response.Redirect("./InviteOwners.aspx?ln=" + leagueName + "&ls=" + leagueSize + "&lid=" + leagueId + "", true);
                }
                else if (selectedValue == 2)
                {
                    Response.Redirect("./RankPresidents.aspx?lid=" + leagueId + "", true);
                }
        }
        private void SetDropDownList()
        {
            try
            {

                ddlScreens.Items.Insert(0, new ListItem(""));
                ddlScreens.Items.Insert(1, new ListItem("Invite League Members"));
                ddlScreens.Items.Insert(2, new ListItem("Rate Presidents"));
                ddlScreens.DataBind();
            }
            catch
            {
                MessageBox("Unable to bind drop down list");
            }
        }
    }
}