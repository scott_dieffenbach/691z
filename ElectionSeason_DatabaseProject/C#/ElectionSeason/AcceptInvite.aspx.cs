﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ElectionSeason.Utilities;

namespace ElectionSeason
{
    public partial class AcceptInvite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string leagueid = Request.QueryString["lid"];
            string leagueName = Request.QueryString["ln"];
            int IdNumber = Convert.ToInt32(Session["UserAccountID"].ToString());

            bool isUpdated = ESDBUtility.SetAcceptInvite(IdNumber, leagueid);
            
            if (isUpdated)
            {
                lblAcceptInvite.Text = leagueName;
            }
            else
            {
                MessageBox("League cannot be accepted due to internal error");
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.btnHome.Click += new EventHandler(btnHome_Click);
            base.OnInit(e);
        }

        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("./Home.aspx", true);
        }
    }
}