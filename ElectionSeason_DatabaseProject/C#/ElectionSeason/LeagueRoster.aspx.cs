﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ElectionSeason
{
    public partial class LeagueRoster : System.Web.UI.Page
    {

        String leagueId = "";
        String lastQuestionHeader = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["UserAccountID"] = 1;
            if (Session["UserAccountID"] == null)
            {
                string strException = "Not authenticated. Please login again";
                MessageBox(strException);
                Response.Redirect("Login.aspx");
            }

            if (leagueId == "")
            {
                leagueId = Request.QueryString["lid"].ToString();
            }
            else
            {
                string strException = "League not found. Please try again.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + strException + "');  window.location='" + Request.ApplicationPath + "leagueconfig.aspx';", true);
            }
        }
        protected void ds_LeagueRoster_OnLoad(object sender, EventArgs e)
        {
            ds_LeagueRoster.SelectParameters["LeagueID"].DefaultValue = leagueId.ToString();
        }

        protected void lnk_returntoLeagueHome_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("LeagueHome.aspx");
        }


        protected void lnk_BeginDraft_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("./DraftPresident.aspx?lid=" + leagueId + "", true);
        }
        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }
        protected string IfNextGroupAddHeader(object sender, EventArgs e)
        {
            String FirstName = Eval("FirstName").ToString();
            String LastName = Eval("LastName").ToString();
            String currentQuestion = Eval("Email").ToString();

            if (lastQuestionHeader != currentQuestion)
            {
                lastQuestionHeader = currentQuestion;
                return String.Format("{0}", "Team roster for " + FirstName + " " + LastName);
            }
            else
            {
                return string.Empty;
            }
        }


        
    }
}