﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountManagement.aspx.cs" Inherits="ElectionSeason.AccountManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Account Management</title>
    <link rel="stylesheet" type="text/css" href="css/ElectionSeason.css" />
</head>
<body id="body">
    <form id="frm_AccountManagement" runat="server">
    <div id="divElectionSeason_AccountManagement">
        <asp:Panel ID="pnl_AccountManagement" runat="server" HorizontalAlign="Left" >
        <img src="logo.jpg" alt="logo" style="height:150px;width:300px;" />
        <asp:ListView ID="lv_AccountManagement" runat="server" DataSourceID="ds_AccountManagement" 
            DataKeyNames="AccountInformationID" EditIndex="0" OnItemUpdating="lv_AccountManagement_ItemUpdating" OnItemUpdated="lv_AccountManagement_ItemUpdated">
            <EditItemTemplate>
                    UserAccessID:
                    <asp:Label Text='<%# Bind("UserAccountID") %>' runat="server" ID="lbl_UserAccountID" /><br />
                    AccountInformationID:
                    <asp:Label Text='<%# Eval("AccountInformationID") %>' runat="server" ID="lbl_AccountInformationID" /><br />
                    FirstName:
                    <asp:TextBox Text='<%# Bind("FirstName") %>' runat="server" ID="txt_FirstName" /><br />
                    LastName:
                    <asp:TextBox Text='<%# Bind("LastName") %>' runat="server" ID="txt_LastName" /><br />
                    PhoneNumber:
                    <asp:TextBox Text='<%# Bind("PhoneNumber") %>' runat="server" ID="txt_PhoneNumber" /><br />
                    Email:
                    <asp:Label Text='<%# Bind("Email") %>' runat="server" ID="lbl_Email" />
                 <br />
                    <asp:Button runat="server" CommandName="Update" Text="Update" ID="UpdateButton" />
                    <asp:Button runat="server" CommandName="Cancel" Text="Cancel" ID="CancelButton" OnClick="btn_Cancel"/>
            </EditItemTemplate>
            <EmptyDataTemplate>
                No data was returned.
            </EmptyDataTemplate>
            <ItemTemplate>
                    UserAccessID:
                    <asp:Label Text='<%# Eval("UserAccountID") %>' runat="server" ID="lbl_UserAccountID" /><br />
                    AccountInformationID:
                    <asp:Label Text='<%# Eval("AccountInformationID") %>' runat="server" ID="lbl_AccountInformationID" /><br />
                    UserName:
                    <asp:Label Text='<%# Eval("Email") %>' runat="server" ID="lbl_UserName" /><br />
                    FirstName:
                    <asp:Label Text='<%# Eval("FirstName") %>' runat="server" ID="lbl_FirstName" /><br />
                    LastName:
                    <asp:Label Text='<%# Eval("LastName") %>' runat="server" ID="lbl_LastName" /><br />
                    PhoneNumber:
                    <asp:Label Text='<%# Eval("PhoneNumber") %>' runat="server" ID="lbl_PhoneNumber" /><br />
                    Email:
                    <asp:Label Text='<%# Eval("Email") %>' runat="server" ID="lbl_Email" /><br />              
            </ItemTemplate>
            <LayoutTemplate>
                <ul runat="server" id="itemPlaceholderContainer" style="">
                    <li runat="server" id="itemPlaceholder" />
                </ul>
                <div style="">
                </div>
            </LayoutTemplate>
        </asp:ListView>
        </asp:Panel>

        <asp:SqlDataSource ID="ds_AccountManagement" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>'
            SelectCommand="SELECT [UserAccountID], [AccountInformationID], [UserPassword], [FirstName], [LastName], [PhoneNumber], [Email] 
            FROM [AccountInformation] WHERE (UserAccountID = @UserAccountID)"
            UpdateCommand="UPDATE AccountInformation SET FirstName = @FirstName, LastName = @LastName, PhoneNumber = @PhoneNumber WHERE UserAccountID = @UserAccountID">
            <SelectParameters>
                <asp:Parameter Name="UserAccountID" DbType="Int64" DefaultValue="1"></asp:Parameter>
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="UserAccountID" DbType="String"></asp:Parameter>
                <asp:Parameter Name="FirstName" DbType="String"></asp:Parameter>
                <asp:Parameter Name="LastName" DbType="String"></asp:Parameter>
                <asp:Parameter Name="PhoneNumber" DbType="String"></asp:Parameter>
            </UpdateParameters>
         </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
