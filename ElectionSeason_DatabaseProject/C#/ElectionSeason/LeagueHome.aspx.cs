﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ElectionSeason.Utilities;

namespace ElectionSeason
{
    public partial class LeagueHome : System.Web.UI.Page
    {
        DataTable dtYourLeagues = null;
        DataTable dtAcceptInvite = null;
        DataTable dtLeagueConfig = null;
         
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected override void OnInit(EventArgs e)
        {
            this.btnCreateLeague.Click += new EventHandler(btnCreateLeague_Click);
            this.ddlYourLeagues.SelectedIndexChanged += new EventHandler(ddlYourLeagues_SelectedIndexChanged);
            this.ddlYourLeagues.AutoPostBack = true;
            this.ddlAcceptInvite.SelectedIndexChanged += new EventHandler(ddlAcceptInvite_SelectedIndexChanged);
            this.ddlAcceptInvite.AutoPostBack = true;
            this.ddlLeagueConfig.SelectedIndexChanged += new EventHandler(ddlLeagueConfig_SelectedIndexChanged);
            this.ddlLeagueConfig.AutoPostBack = true;
            SetDropDownList();
            base.OnInit(e);
        }

        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }

        protected void ddlYourLeagues_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dtYourLeagues.Rows.Count > 0)
            {
                int selectedValue = ddlYourLeagues.SelectedIndex;
                string leagueId = dtYourLeagues.Rows[selectedValue][0].ToString();
                Response.Redirect("./LeagueRoster.aspx?lid=" + leagueId + "", true);
            }
        }

        protected void ddlAcceptInvite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dtAcceptInvite.Rows.Count > 0)
            {
                int selectedValue = ddlAcceptInvite.SelectedIndex;
                string leagueID = dtAcceptInvite.Rows[selectedValue][0].ToString();
                string leagueName = dtAcceptInvite.Rows[selectedValue][2].ToString();
                Response.Redirect("./AcceptInvite.aspx?lid=" + leagueID + "&ln=" + leagueName + "", true);
            }
        }

        protected void ddlLeagueConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dtLeagueConfig.Rows.Count > 0)
            {
                int selectedValue = ddlLeagueConfig.SelectedIndex;
                string leagueID = dtLeagueConfig.Rows[selectedValue][0].ToString();
                string leagueName = dtLeagueConfig.Rows[selectedValue][1].ToString();
                int IdNumber = Convert.ToInt32(Session["UserAccountID"].ToString());
                Response.Redirect("./LeagueConfig.aspx?lid=" + leagueID + "&ln=" + leagueName + "", true);
            }
        }

        private void SetDropDownList()
        {
            try
            {
                int IdNumber = Convert.ToInt32(Session["UserAccountID"].ToString());

                dtYourLeagues = ESDBUtility.GetUserLeagues(IdNumber);
                if (dtYourLeagues.Rows.Count > 0)
                {
                    DataRow dr = dtYourLeagues.NewRow();
                    dtYourLeagues.Rows.InsertAt(dr, 0);

                    this.ddlYourLeagues.DataSource = dtYourLeagues;
                    this.ddlYourLeagues.DataTextField = dtYourLeagues.Columns[2].ToString();
                    this.ddlYourLeagues.DataBind();
                }
                else
                {
                    lblYourLeagues.Visible = false;
                    ddlYourLeagues.Visible = false;
                }

                dtAcceptInvite = ESDBUtility.GetUserInvites(IdNumber);
                if (dtAcceptInvite.Rows.Count > 0)
                {
                    DataRow dr = dtAcceptInvite.NewRow();
                    dtAcceptInvite.Rows.InsertAt(dr, 0);

                    this.ddlAcceptInvite.DataSource = dtAcceptInvite;
                    ddlAcceptInvite.DataTextField = dtAcceptInvite.Columns[2].ToString();
                    ddlAcceptInvite.DataBind();
                }
                else
                {
                    lblAcceptInvite.Visible = false;
                    ddlAcceptInvite.Visible = false;
                }

                dtLeagueConfig = ESDBUtility.GetUserCommish(IdNumber);
                if (dtLeagueConfig.Rows.Count > 0)
                {
                    DataRow dr = dtLeagueConfig.NewRow();
                    dtLeagueConfig.Rows.InsertAt(dr, 0);

                    this.ddlLeagueConfig.DataSource = dtLeagueConfig;
                    ddlLeagueConfig.DataTextField = dtLeagueConfig.Columns[1].ToString();
                    ddlLeagueConfig.DataBind();
                }
                else
                {
                    lblCommish.Visible = false;
                    ddlLeagueConfig.Visible = false;
                }
            }
            catch
            {
                MessageBox("Unable to bind drop down list");
            }
        }

        protected static DataTable InsertBlank(DataTable dt)
        {
            DataRow dr = dt.NewRow();

            dr[0] = "";
            dr[1] = "";
            dr[2] = "";

            dt.Rows.InsertAt(dr, 0);

            return dt;
        }

        protected void btnCreateLeague_Click(object sender, EventArgs e)
        {
            Response.Redirect("./LeagueCreation.aspx", true);
        }
    }
}