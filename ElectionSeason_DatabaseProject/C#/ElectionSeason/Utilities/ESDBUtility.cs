﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace ElectionSeason.Utilities
{
    public class ESDBUtility
    {
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ES_DB_Connection"].ToString();
        }
        private static DataTable Select(string query)
        {
            string connectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(connectionString);
            DataTable dt = null;
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                dt = new DataTable();
                da.Fill(dt);
                connection.Close();

            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        private static bool Update(string query)
        {
            bool isUpdated = false;
            try
            {
                string connectionString = GetConnectionString();
                SqlConnection connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(query, connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
                isUpdated = true;
            }
            catch
            {
                isUpdated = false;
            }
            return isUpdated;
        }
        public static string IsUser(string userName, string password)
        {
            string idNumber = "";
            string query = String.Format("select UserAccountID from UserAccess where UserName = '{0}' and UserPassword = '{1}' and LockedOut = 0", userName, password);
            DataTable dt = Select(query);
            if (dt != null && dt.Rows.Count > 0)
            {
                idNumber = dt.Rows[0][0].ToString();
            }
            return idNumber;
        }
        public static string GetEmailAddress(string userName)
        {
            string emailAddress = "";
            string query = String.Format("query {0}", userName);
            DataTable dt = Select(query);
            if (dt != null && dt.Rows.Count > 0)
            {
                emailAddress = dt.Rows[0][0].ToString();
            }
            return emailAddress;
        }
        public static bool UpdatePassword(string userName, string password)
        {
            bool isUpdated = false;
            string query = String.Format("update UserAccess set password = {0} where UserName = {1}", password, userName);
            isUpdated = Update(query);
            return isUpdated;
        }
        public static DataTable GetScreens()
        {
            string query = "select id,screen_desc,screen from ScreenDefinition";
            DataTable dt = Select(query);
            return dt;
        }
        //Jason Added 3/15/15
        public static bool CreateLeague(string leagueName, string leagueSize, string commish)
        {
            bool isUpdated = false;
            string query = String.Format("Insert into LeagueInfo (LeagueName, LeagueSize, LeagueCommish) values ('{0}', '{1}', '{2}')", leagueName, leagueSize, commish);
            isUpdated = Update(query);
            return isUpdated;
        }

        public static int CheckForSameLeagueName(string leagueName)
        {
            int number;
            string query = String.Format("select count(*) from LeagueInfo where leagueName = '{0}'", leagueName);
            number = SelectCount(query);
            return number;
        }

        public static int GetLeagueId(string leagueName)
        {
            int number;
            string query = String.Format("select LeagueID from LeagueInfo where leagueName = '{0}'", leagueName);
            DataTable dt = Select(query);
            Int64 getLeagueID = dt.Rows[0].Field<Int64>("LeagueID");
            number = Convert.ToInt16(getLeagueID);
            return number;


        }

        public static bool CreateLeagueAssoc(int leagueID, string userAccountID, char invite)
        {
            bool isUpdated = false;
            string query = String.Format("Insert into LeagueAssoc (LeagueID, UserAccountID, InviteAccepted) values ('{0}', '{1}', '{2}')", leagueID, userAccountID, invite);
            isUpdated = Update(query);
            return isUpdated;
        }

        private static int SelectCount(string query)
        {
            string connectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(connectionString);
            DataTable dt = null;
            int number = -1;
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                dt = new DataTable();
                da.Fill(dt);
                connection.Close();
                number = dt.Rows[0].Field<int>("Column1");

            }
            catch
            {
                dt = null;
            }
            return number;
        }
        // Jason 03/18/15
        public static int CheckForSameEmail(string emailAddy)
        {
            int number;
            string query = String.Format("select count(*) from AccountInformation where Email = '{0}'", emailAddy);
            number = SelectCount(query);
            return number;
        }
        public static bool CreateInvitedUser(string firstName, string lastName, string emailAddy)
        {
            bool isUpdated = false;
            string query = String.Format("Insert into AccountInformation (FirstName, LastName, Email) values ('{0}', '{1}', '{2}')", firstName, lastName, emailAddy);
            isUpdated = Update(query);
            return isUpdated;
        }
        public static int GetUserAccountID(string emailAddy)
        {
            int number;
            string query = String.Format("select AccountInformationID from AccountInformation where Email = '{0}'", emailAddy);
            DataTable dt = Select(query);
            Int64 getLeagueID = dt.Rows[0].Field<Int64>("AccountInformationID");
            number = Convert.ToInt16(getLeagueID);
            return number;
        }
        //Jason 03/21/15
        public static string GetPresidentName(int presID)
        {
            string presName;
            string query = String.Format("select PresidentName from Presidents where PresidentNum = '{0}'", presID);
            DataTable dt = Select(query);
            presName = dt.Rows[0].Field<string>("PresidentName");
            return presName;
        }
        public static bool SetInitialPresidentStats(int leagueId)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            string query = String.Format("select '{0}', PresidentID from Presidents", leagueId);
            dt = Select(query);
            dt.Columns[0].ColumnName = "LeagueID";
            dt.Columns.Add("Speech");
            dt.Columns.Add("Campaigning");
            dt.Columns.Add("Foreign");
            dt.Columns.Add("Domestic");
            dt.Columns.Add("Economic");
            dt.Columns.Add("Military");
            dt.Columns.Add("Legal");
            dt.Columns.Add("Educational");
            dt.Columns.Add("AVAILABLE");
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName == "PresidentID" || col.ColumnName == "LeagueID" || col.ColumnName == "AVAILABLE")
                    {

                    }
                    else
                    {
                        dr[col] = "50";
                    }
                    if (col.ColumnName == "AVAILABLE")
                    {
                        dr[col] = 'Y';
                    }
                }
            }

            bool isUpdated = false;

            foreach (DataRow row in dt.Rows)
            {
                string queryInsert = String.Format("insert into PresidentRank (leagueId, PresidentID, Speech, Campaigning, [Foreign], Domestic, Economic, Military, Legal, Educational, Available) Values ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},'{10}')", row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]);

                isUpdated = Update(queryInsert);
            }

            return isUpdated;
        }

        public static void SetPresidentStats(int leagueID, DataTable dt)
        {
            bool isUpdated = false;

            foreach (DataRow row in dt.Rows)
            {
                string queryInsert = String.Format("update PresidentRank Set leagueId = '{0}', PresidentID = '{1}', Speech = '{2}', Campaigning = '{3}', [Foreign] = '{4}', Domestic = '{5}', Economic = '{6}', Military = '{7}', Legal = '{8}', Educational = '{9}', Available = '{10}' Where LeagueID = '{0}' AND PresidentID = '{1}'", row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]);

                isUpdated = Update(queryInsert);
            }
        }

        public static DataTable GetPresidentStats(int leagueID)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            string query = String.Format("select * from PresidentRank where leagueID = '{0}' order by PresidentID ASC", leagueID);
            dt = Select(query);

            return dt;
        }

        //JES 03/29/15
        public static DataTable GetUserLeagues(int IdNumber)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            string query = String.Format("select a.LeagueID, a.UserAccountID, i.LeagueName from LeagueAssoc a, LeagueInfo i where a.LeagueID=i.LeagueID AND a.UserAccountID = '{0}' and a.InviteAccepted = 'Y' order by LeagueID ASC", IdNumber);
            dt = Select(query);

            return dt;
        }

        public static DataTable GetUserInvites(int IdNumber)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            string query = String.Format("select a.LeagueID, a.UserAccountID, i.LeagueName from LeagueAssoc a, LeagueInfo i where a.LeagueID=i.LeagueID AND a.UserAccountID = '{0}' and a.InviteAccepted = 'N' order by LeagueID ASC", IdNumber);
            dt = Select(query);

            return dt;
        }

        public static DataTable GetUserCommish(int IdNumber)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            string query = String.Format("select LeagueID, LeagueName from LeagueInfo where LeagueCommish = '{0}' order by LeagueID ASC", IdNumber);
            dt = Select(query);

            return dt;
        }

        public static bool SetAcceptInvite(int IdNumber, string leagueID)
        {
            bool isUpdated = false;

            string queryInsert = String.Format("update LeagueAssoc Set InviteAccepted = 'Y' where LeagueID = '{0}' AND UserAccountID = '{1}'", leagueID, IdNumber);

            isUpdated = Update(queryInsert);

            return isUpdated;
        }

        public static int GetLeagueSize(int leagueID)
        {
            int leagueSize = 0;

            string query = String.Format("select LeagueSize from LeagueInfo where LeagueID = '{0}'", leagueID);

            DataTable dt = Select(query);
            int getLeagueSize = dt.Rows[0].Field<int>("LeagueSize");
            leagueSize = Convert.ToInt16(getLeagueSize);
            return leagueSize;
        }

        public static DataTable GetLeagueInvities(int leagueID)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            string query = String.Format("select a.FirstName, a.LastName, a.Email, l.LeagueID from AccountInformation a, LeagueAssoc l where a.AccountInformationID=l.UserAccountID AND l.LeagueID = '{0}'", leagueID);
            dt = Select(query);

            return dt;
        }
    }
}