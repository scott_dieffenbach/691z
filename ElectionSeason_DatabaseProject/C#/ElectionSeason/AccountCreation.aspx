﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountCreation.aspx.cs" Inherits="ElectionSeason_Web.AccountInformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Account Creation</title>
    <link rel="stylesheet" type="text/css" href="css/ElectionSeason.css" />
</head>
<body id="body">
    <form id="frm_AccountInformation" runat="server">
        <asp:ScriptManager ID="sctMan_AccountCreation" runat="server">
        </asp:ScriptManager>
        <div id="divElectionSeason_AccountCreation">
            <asp:Panel ID="pnl_CreateAccount" runat="server" HorizontalAlign="Left" >
            <img src="logo.jpg" alt="logo" style="height:150px;width:350px;" />

                <table runat="server" >
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="valSum_CreateAccount" runat="server" 
                                ShowMessageBox="false" HeaderText="The following issues need resolved:"
                                DisplayMode="BulletList" ShowSummary="true" ForeColor="Red"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_Email" runat="server" Text="Enter email address: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_Email" runat="server" TextMode="Email"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqVal_Email" runat="server" ControlToValidate="txt_Email"
                                   ErrorMessage="Email is a required field" ForeColor="Red">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regExp_ValidEmailAddress" runat="server" ControlToValidate="txt_Email"
                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ErrorMessage="Must be a valid email address. "
                                Display="None"
                                ForeColor="Red" >*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_Password" runat="server" Text="Enter valid password: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_Password" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqVal_Password" runat="server" ControlToValidate="txt_Password"
                                   ErrorMessage="Enter a password" ForeColor="Red">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <asp:Label ID="lbl_ConfirmPassword" runat="server" Text="Confirm password" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqVal_ConfirmPassword" runat="server" ControlToValidate="txt_Password"
                                   ErrorMessage="Password must be comfirmed" ForeColor="Red">*</asp:RequiredFieldValidator>

                            <asp:CompareValidator ID="comVal_ComparePasswords" runat="server" 
                                ControlToValidate="txt_ConfirmPassword" ControlToCompare="txt_Password"
                                ErrorMessage="Passwords to not match." ForeColor="Red">*</asp:CompareValidator>
<%--                            <asp:RegularExpressionValidator ID="regExp_ValidPassword" runat="server" ControlToValidate="txt_Password"
                                ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,10}"
                                ErrorMessage="Password must be 8 to 10 characters and must contain 1 UpperCase, 1 LowerCase, 1 Number and 1 Special Character"
                                Display="None"
                                ForeColor="Red">*</asp:RegularExpressionValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btn_CreateAccount" runat="server" Text="Submit" OnClick="btn_CreateAccount_Click"/>
                        </td>
                        <td>
                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancel"/>

                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:SqlDataSource ID="ds_CreateUserAccount" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>'
                SelectCommand="SELECT UserAccountID, UserName FROM UserAccess WHERE (UserName = @UserName)"
                InsertCommand="INSERT INTO UserAccess(UserName, UserPassword, LockedOut) VALUES (@UserName,@UserPassword,@LockedOut)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txt_Email" PropertyName="Text" Name="UserName" Type="String"></asp:ControlParameter>
                </SelectParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txt_Email" PropertyName="Text" Name="UserName" Type="String"></asp:ControlParameter>
                    <asp:ControlParameter ControlID="txt_Password" PropertyName="Text" Name="UserPassword" Type="String"></asp:ControlParameter>
                    <asp:Parameter Name="LockedOut" Type="Boolean" DefaultValue="false"></asp:Parameter>
                 </InsertParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
