﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ElectionSeason.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/login.css"/>
</head>
<body id="body">
    <form id="form1" runat="server">
    <div id="divLogin">
        <img src="logo.jpg" alt="logo" style="height:150px;width:300px;" />
        <asp:TextBox ID="tbUsername" runat="server" cssclass="textbox"></asp:TextBox><br /><br />
        <asp:TextBox ID="tbPassword" runat="server" CssClass="textbox" textmode="Password"></asp:TextBox><br /><br />
        <asp:Button ID="btnLogin" runat="server" Text="Log In" CssClass="button" /><br />
        <asp:LinkButton ID="lbForgotPassword" runat="server" CssClass="ForgotPassword">ForgotPassword</asp:LinkButton><br /><br />
        <asp:Label ID="lblWarning" runat="server" Text="Unauthorized access is prohibited by law" CssClass="label"></asp:Label><br />
        <asp:Label ID="lblCopyright" runat="server" Text="Copyright © ES Software" CssClass="label"></asp:Label>
    </div>
    </form>
</body>
</html>
