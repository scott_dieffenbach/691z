﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using ElectionSeason.Utilities;
using System.Data;
using System.IO;



namespace ElectionSeason
{
    public partial class RankPresidents : System.Web.UI.Page
    {
        int currentPresident = 1;
        //int leagueID = 34
        
        //string SessionID = "1";
        //temp until link from league config page
        DataTable dt = new DataTable();
        string SessionID = "0"; // Session["IdNumber"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            

            //HttpContext.Current.Session["UserAccountID"] = 1;
            if (Session["UserAccountID"] == null)
            {
                string strException = "Not authenticated. Please login again";
                MessageBox(strException);
                Response.Redirect("Login.aspx");
            }
            else
            {

                SessionID = Session["UserAccountID"].ToString();
            }

            try
            {
                if (!IsPostBack)
                {
                    imgPresident.ImageUrl = "images/1.jpg";

                   

                    string currentPresidentCache = ("RankPres" + SessionID);
                    Cache.Insert(currentPresidentCache, currentPresident);

                    GetSliderValuesFromDataTable();
                    GetSliderValues();
                }

            }
            catch
            {
                MessageBox("it failed");
            }

        }
        protected override void OnInit(EventArgs e)
        {
            this.btnMoveLeft.Click += new EventHandler(btnMoveLeft_Click);
            this.btnMoveRight.Click += new EventHandler(btnMoveRight_Click);
            this.btnSave.Click += new EventHandler(btnSave_Click);
            this.btnRandomize.Click += new EventHandler(btnRandomize_Click);
            this.btnRandomizeAll.Click += new EventHandler(btnRandomizeAll_Click);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
            base.OnInit(e);
        }

        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("./LeagueHome.aspx", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            GetSliderValues();

            int leagueID = Convert.ToInt16(Request.QueryString["lid"]);
            //string SessionID = "1"; // Session["IdNumber"].ToString();
            SessionID = Session["UserAccountID"].ToString();

            string datatablecache = ("datatable" + SessionID);

            dt = (DataTable)Cache.Get(datatablecache);

            ESDBUtility.SetPresidentStats(leagueID, dt);

            GetSliderValuesFromDataTable();
        }

        protected void btnRandomize_Click(object sender, EventArgs e)
        {
            Random r = new Random();

            //string SessionID = "1"; // Session["IdNumber"].ToString();
            SessionID = Session["UserAccountID"].ToString();

            string datatablecache = ("datatable" + SessionID);

            dt = (DataTable)Cache.Get(datatablecache);

            string currentPresidentCache = ("RankPres" + SessionID);
            currentPresident = Convert.ToInt16(Cache.Get(currentPresidentCache));

            DataRow[] result = dt.Select("PresidentID = '" + currentPresident + "'");
            foreach (DataRow row in result)
            {
                row["Speech"] = r.Next(1, 101);
                row["Campaigning"] = r.Next(1, 101);
                row["Foreign"] = r.Next(1, 101);
                row["Domestic"] = r.Next(1, 101);
                row["Economic"] = r.Next(1, 101);
                row["Military"] = r.Next(1, 101);
                row["Legal"] = r.Next(1, 101);
                row["Educational"] = r.Next(1, 101);
            }

            Cache.Insert(datatablecache, dt);

            GetSliderValuesFromDataTable();
        }

        protected void btnRandomizeAll_Click(object sender, EventArgs e)
        {
            Random r = new Random();

            //string SessionID = "1"; // Session["IdNumber"].ToString();
            SessionID = Session["UserAccountID"].ToString();

            string datatablecache = ("datatable" + SessionID);
            //DataTable dt = new DataTable();
            dt = (DataTable)Cache.Get(datatablecache);

            string currentPresidentCache = ("RankPres" + SessionID);
            currentPresident = Convert.ToInt16(Cache.Get(currentPresidentCache));

            foreach (DataRow row in dt.Rows)
            {
                row["Speech"] = r.Next(1, 101);
                row["Campaigning"] = r.Next(1, 101);
                row["Foreign"] = r.Next(1, 101);
                row["Domestic"] = r.Next(1, 101);
                row["Economic"] = r.Next(1, 101);
                row["Military"] = r.Next(1, 101);
                row["Legal"] = r.Next(1, 101);
                row["Educational"] = r.Next(1, 101);
            }

            Cache.Insert(datatablecache, dt);

            GetSliderValuesFromDataTable();
        }

        protected void btnMoveLeft_Click(object sender, EventArgs e)
        {
            GetSliderValues();

            //string SessionID = "1"; // Session["IdNumber"].ToString();
            SessionID = Session["UserAccountID"].ToString();

            string currentPresidentCache = ("RankPres" + SessionID);

            if (Cache.Get(currentPresidentCache) != null)
                currentPresident = Convert.ToInt16(Cache.Get(currentPresidentCache));

            if (currentPresident == 1)
            {
                currentPresident = 44;
            }
            else
            {
                currentPresident = currentPresident - 1;

            }

            lblPresName.Text = ESDBUtility.GetPresidentName(currentPresident);
            lblPresNum.Text = currentPresident.ToString();
            string imgChange = String.Format("images//{0}.jpg", currentPresident);
            imgPresident.ImageUrl = imgChange;

            Cache.Insert(currentPresidentCache, currentPresident);

            GetSliderValuesFromDataTable();
        }

        protected void btnMoveRight_Click(object sender, EventArgs e)
        {

            GetSliderValues();

            //string SessionID = "1"; // Session["IdNumber"].ToString();
            SessionID = Session["UserAccountID"].ToString();

            string currentPresidentCache = ("RankPres" + SessionID);

            if (Cache.Get(currentPresidentCache) != null)
                currentPresident = Convert.ToInt16(Cache.Get(currentPresidentCache));

            if (currentPresident == 44)
            {
                currentPresident = 1;
            }
            else
            {
                currentPresident = currentPresident + 1;
            }

            lblPresName.Text = ESDBUtility.GetPresidentName(currentPresident);
            lblPresNum.Text = currentPresident.ToString();
            string imgChange = String.Format("images//{0}.jpg", currentPresident);
            imgPresident.ImageUrl = imgChange;

            Cache.Insert(currentPresidentCache, currentPresident);

            GetSliderValuesFromDataTable();
        }

        protected void GetSliderValuesFromDataTable()
        {
            //string SessionID = "1"; // Session["IdNumber"].ToString();
            SessionID = Session["UserAccountID"].ToString();

            string datatablecache = ("datatable" + SessionID);
            int leagueID = Convert.ToInt16(Request.QueryString["lid"]);
           
            if (Cache.Get(datatablecache) != null)
            {
                dt = (DataTable)Cache.Get(datatablecache);
            }
            else
            {
                dt = ESDBUtility.GetPresidentStats(leagueID);
            }

            string currentPresidentCache = ("RankPres" + SessionID);
            currentPresident = Convert.ToInt16(Cache.Get(currentPresidentCache));

            int speechInt = (from DataRow dr in dt.Rows
                      where (int)dr["PresidentID"] == currentPresident
                      select (int)dr["Speech"]).FirstOrDefault();
            string speechValue = Convert.ToString(speechInt);
            speeches.Value = (speechValue);
            speech.InnerText = speechValue;

            int campaignInt = (from DataRow dr in dt.Rows
                               where (int)dr["PresidentID"] == currentPresident
                               select (int)dr["Campaigning"]).FirstOrDefault();
            string campaignValue = Convert.ToString(campaignInt);
            campaigning.Value = (campaignValue);
            campaign.InnerText = campaignValue;

            int foreignInt = (from DataRow dr in dt.Rows
                              where (int)dr["PresidentID"] == currentPresident
                              select (int)dr["Foreign"]).FirstOrDefault();
            string foreignValue = Convert.ToString(foreignInt);
            foreignaffairs.Value = (foreignValue);
            foreign.InnerText = foreignValue;

            int domesticInt = (from DataRow dr in dt.Rows
                               where (int)dr["PresidentID"] == currentPresident
                               select (int)dr["Domestic"]).FirstOrDefault();
            string domesticValue = Convert.ToString(domesticInt);
            domesticaffairs.Value = (domesticValue);
            domestic.InnerText = domesticValue;

            int economicInt = (from DataRow dr in dt.Rows
                               where (int)dr["PresidentID"] == currentPresident
                               select (int)dr["Economic"]).FirstOrDefault();
            string economicValue = Convert.ToString(economicInt);
            economicissues.Value = (economicValue);
            economic.InnerText = economicValue;

            int militaryInt = (from DataRow dr in dt.Rows
                               where (int)dr["PresidentID"] == currentPresident
                               select (int)dr["Military"]).FirstOrDefault();
            string militaryValue = Convert.ToString(militaryInt);
            militaryaffairs.Value = (militaryValue);
            military.InnerText = militaryValue;

            int legalInt = (from DataRow dr in dt.Rows
                            where (int)dr["PresidentID"] == currentPresident
                            select (int)dr["Legal"]).FirstOrDefault();
            string legalValue = Convert.ToString(legalInt);
            legalissues.Value = (legalValue);
            legal.InnerText = legalValue;

            int educationInt = (from DataRow dr in dt.Rows
                                where (int)dr["PresidentID"] == currentPresident
                                select (int)dr["Educational"]).FirstOrDefault();
            string educationValue = Convert.ToString(educationInt);
            educationissues.Value = (educationValue);
            education.InnerText = educationValue;

            Cache.Insert(datatablecache, dt);
        }

        protected void GetSliderValues()
        {
            //string SessionID = "1"; // Session["IdNumber"].ToString();
            SessionID = Session["UserAccountID"].ToString();

            string datatablecache = ("datatable" + SessionID);

            if ((DataTable)Cache.Get(datatablecache) != null)
            {

                dt = (DataTable)Cache.Get(datatablecache);
                string currentPresidentCache = ("RankPres" + SessionID);
                currentPresident = Convert.ToInt16(Cache.Get(currentPresidentCache));

                DataRow[] result = dt.Select("PresidentID = '" + currentPresident + "'");
                foreach (DataRow row in result)
                {
                    row["Speech"] = speeches.Value;
                    row["Campaigning"] = campaigning.Value;
                    row["Foreign"] = foreignaffairs.Value;
                    row["Domestic"] = domesticaffairs.Value;
                    row["Economic"] = economicissues.Value;
                    row["Military"] = militaryaffairs.Value;
                    row["Legal"] = legalissues.Value;
                    row["Educational"] = educationissues.Value;
                }
                Cache.Insert(datatablecache, dt);
            }
        }

    }


}