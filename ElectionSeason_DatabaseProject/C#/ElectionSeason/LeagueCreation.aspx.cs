﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ElectionSeason.Utilities;
using System.Data;

namespace ElectionSeason
{
    public partial class LeagueCreation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnInit(EventArgs e)
        {
            this.btnCreateLeague.Click += new EventHandler(btnCreateLeague_Click);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
            base.OnInit(e);
            
        }
        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }

        protected void btnCreateLeague_Click(object sender, EventArgs e)
        {
            string leagueName = tbLeagueName.Text;

            int n;
            bool isNumeric = int.TryParse(tbLeagueSize.Text, out n);

            if (!isNumeric)
            {
                MessageBox("Please enter a number from 4 to 8 for league size");
                tbLeagueSize.Text = " ";
            }
            else
            {
                string leagueSize = tbLeagueSize.Text;

                string Commish = "1"; // Session["IdNumber"].ToString();
                //string Commish = "1";

                if (String.IsNullOrWhiteSpace(leagueName) || String.IsNullOrWhiteSpace(leagueSize))
                {
                    if (String.IsNullOrWhiteSpace(leagueName))
                    {
                        MessageBox("Please enter a league name");
                    }
                    else
                    {
                        MessageBox("Please enter a league size");
                    }
                }
                else
                {
                    int leagueSizeInt = Convert.ToInt16(leagueSize);
                    if (leagueSizeInt <= 3 || leagueSizeInt >= 9)
                    {
                        MessageBox("Please enter a number from 4 to 8 for league size");
                        tbLeagueSize.Text = " ";
                    }
                    else
                    {
                        int numberOfLeagues = ESDBUtility.CheckForSameLeagueName(leagueName);
                        if (numberOfLeagues == -1)
                        {
                            MessageBox("Internal Error, please contact application support");
                            tbLeagueName.Text = " ";
                            tbLeagueSize.Text = " ";
                        }
                        else if (numberOfLeagues > 0)
                        {
                            MessageBox("League Name already exist, enter a new name.");
                            tbLeagueName.Text = " ";
                        }
                        else
                        {
                            bool isUpdated = ESDBUtility.CreateLeague(leagueName, leagueSize, Commish);
                            int leagueId = ESDBUtility.GetLeagueId(leagueName);
                            char Invite = 'Y';
                            bool isUpdatedLeagueAssoc = ESDBUtility.CreateLeagueAssoc(leagueId, Commish, Invite);
                            bool isDataTable = ESDBUtility.SetInitialPresidentStats(leagueId);
                            if (isUpdated && isUpdatedLeagueAssoc)
                            {
                                MessageBox("League Creation Complete");

                                Response.Redirect("./InviteOwners.aspx?ln=" + leagueName + "&ls="+ leagueSize + "&lid="+ leagueId + "&Com=" + Commish + "",true);
                            }
                            else
                            {
                                if (!isUpdated && !isUpdatedLeagueAssoc)
                                {
                                    MessageBox("League creation failed due to database unable to update LeagueInfo and LeagueAssoc tables");
                                }
                                else if (!isUpdated)
                                {
                                    MessageBox("League creation Failed due to database unable to update LeagueInfo");
                                }
                                else
                                {
                                    MessageBox("League creation Failed due to database unable to update LeagueAssoc");
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("./LeagueHome.aspx", true);
        }

    }
}