﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeagueRoster.aspx.cs" Inherits="ElectionSeason.LeagueRoster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>League Roster</title>
    <link rel="stylesheet" type="text/css" href="css/ElectionSeason.css" />
</head>
<body>
    <form id="frm_LeagueRoster" runat="server">
    <div>
      <asp:Panel ID="pnl_LeagueRoster" runat="server" HorizontalAlign="Left" >

       <img src="logo.jpg" alt="logo" style="height:150px;width:350px;" />

        <asp:SqlDataSource ID="ds_LeagueRoster" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>' OnLoad="ds_LeagueRoster_OnLoad"
            SelectCommand="SELECT lr.[LeagueRosterID],lr.[LeagueID],lr.[PresidentID],lr.[UserAccountID],ai.[FirstName],
	            ai.[LastName],ai.[Email],p.[PresidentID],
                p.[PresidentName],p.[PresidentNum],pr.[Speech],pr.[Campaigning],pr.[Foreign],pr.[Domestic],
	            pr.[Economic],pr.[Military],pr.[Legal],pr.[Educational],pr.[AVAILABLE],pr.[TEAMID]
            FROM 
	            [dbo].[LeagueRoster] lr
	            JOIN [dbo].[Presidents] p ON lr.[PresidentID] = p.[PresidentID] 
	            JOIN [dbo].[PresidentRank] pr ON pr.[PresidentID] = p.[PresidentID]
	            JOIN [dbo].[AccountInformation] ai ON lr.[UserAccountID] = ai.[UserAccountID]
            WHERE 
	            pr.[LeagueID] = @LeagueID
				AND 
				EXISTS(SELECT [LeagueRosterID]  FROM [dbo].[LeagueRoster] WHERE [LeagueID] = @LeagueID)
            ORDER BY
                lr.[UserAccountID]">
            <SelectParameters>
                <asp:Parameter Name="LeagueID" DbType="Int64" ></asp:Parameter>
            </SelectParameters>
        </asp:SqlDataSource>
        <table border="1">
            <tr>
                <td>
                    <asp:LinkButton ID="lnk_returntoLeagueHome" runat="server" Text="Return to League Home" OnClick="lnk_returntoLeagueHome_OnClick"></asp:LinkButton>
                    <br/>
                    <asp:LinkButton ID="lnk_BeginDraft" runat="server" Text="Go to Draft" OnClick="lnk_BeginDraft_OnClick"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ListView ID="lv_LeagueRoster" runat="server" DataSourceID="ds_LeagueRoster">
            <EditItemTemplate>
                <tr style="">
                    <td>
                        <asp:Button runat="server" CommandName="Update" Text="Update" ID="UpdateButton" />
                        <asp:Button runat="server" CommandName="Cancel" Text="Cancel" ID="CancelButton" />
                    </td>
                    <td>
                        <asp:Label Text='<%# Eval("LeagueRosterID") %>' runat="server" ID="LeagueRosterIDLabel1" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("LeagueID") %>' runat="server" ID="LeagueIDTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentID") %>' runat="server" ID="PresidentIDTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("UserAccountID") %>' runat="server" ID="UserAccountIDTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("FirstName") %>' runat="server" ID="FirstNameTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("LastName") %>' runat="server" ID="LastNameTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Email") %>' runat="server" ID="EmailTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentID1") %>' runat="server" ID="PresidentID1TextBox" /></td>
                    <td>
                        <asp:Image ID="img_PresidentImage" ImageUrl='<%# string.Format("~/images/{0}.jpg", Eval("PresidentID")) %>' runat="server"  Width="40px" Height="40px" />                                 
                    </td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentName") %>' runat="server" ID="PresidentNameTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentNum") %>' runat="server" ID="PresidentNumTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Speech") %>' runat="server" ID="SpeechTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Campaigning") %>' runat="server" ID="CampaigningTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Foreign") %>' runat="server" ID="ForeignTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Domestic") %>' runat="server" ID="DomesticTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Economic") %>' runat="server" ID="EconomicTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Military") %>' runat="server" ID="MilitaryTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Legal") %>' runat="server" ID="LegalTextBox" /></td>
                    <td id="MilitaryLabel" text='<%# Eval("Military") %>'>
                        <asp:TextBox Text='<%# Bind("Educational") %>' runat="server" ID="EducationalTextBox" /></td>
                    <td id="LegalLabel" text='<%# Eval("Legal") %>'>
                        <asp:TextBox Text='<%# Bind("AVAILABLE") %>' runat="server" ID="AVAILABLETextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("TEAMID") %>' runat="server" ID="TEAMIDTextBox" /></td>
                </tr>
            </EditItemTemplate>
            <EmptyDataTemplate>
                <table runat="server" style="">
                    <tr>
                        <td>Draft not started. No data was returned.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <InsertItemTemplate>
                <tr style="">
                    <td>
                        <asp:Button runat="server" CommandName="Insert" Text="Insert" ID="InsertButton" />
                        <asp:Button runat="server" CommandName="Cancel" Text="Clear" ID="CancelButton" />
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox Text='<%# Bind("LeagueID") %>' runat="server" ID="LeagueIDTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentID") %>' runat="server" ID="PresidentIDTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("UserAccountID") %>' runat="server" ID="UserAccountIDTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("FirstName") %>' runat="server" ID="FirstNameTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("LastName") %>' runat="server" ID="LastNameTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Email") %>' runat="server" ID="EmailTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentID1") %>' runat="server" ID="PresidentID1TextBox" /></td>
                    <td>
                        <asp:Image ID="img_PresidentImage" ImageUrl='<%# string.Format("~/images/{0}.jpg", Eval("PresidentID")) %>' runat="server"  Width="40px" Height="40px" />                                 
                    </td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentName") %>' runat="server" ID="PresidentNameTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("PresidentNum") %>' runat="server" ID="PresidentNumTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Speech") %>' runat="server" ID="SpeechTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Campaigning") %>' runat="server" ID="CampaigningTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Foreign") %>' runat="server" ID="ForeignTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Domestic") %>' runat="server" ID="DomesticTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Economic") %>' runat="server" ID="EconomicTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Military") %>' runat="server" ID="MilitaryTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Legal") %>' runat="server" ID="LegalTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("Educational") %>' runat="server" ID="EducationalTextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("AVAILABLE") %>' runat="server" ID="AVAILABLETextBox" /></td>
                    <td>
                        <asp:TextBox Text='<%# Bind("TEAMID") %>' runat="server" ID="TEAMIDTextBox" /></td>
                </tr>
            </InsertItemTemplate>
            <ItemTemplate>
                <tr style="">
                    <th colspan="9" style="text-align: left" class="panel-heading">
                        <asp:Label Text='<%# IfNextGroupAddHeader(sender, e)%>' runat="server" ID="QuestionLabel" />
                    </th>
                </tr>
                <tr>
                    <td>
                        &nbsp
                    </td>
                    <td>
                        <asp:Image ID="img_PresidentImage" ImageUrl='<%# string.Format("~/images/{0}.jpg", Eval("PresidentID")) %>' runat="server"  Width="40px" Height="40px" />                                 
                    </td>
                    <td>
                        <asp:Label Text='<%# Eval("PresidentName") %>' runat="server" ID="PresidentNameLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Speech") %>' runat="server" ID="SpeechLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Campaigning") %>' runat="server" ID="CampaigningLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Foreign") %>' runat="server" ID="ForeignLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Domestic") %>' runat="server" ID="DomesticLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Economic") %>' runat="server" ID="EconomicLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Military") %>' runat="server" ID="MilitaryLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Legal") %>' runat="server" ID="LegalLabel" /></td>
                    <td style="text-align: center">
                        <asp:Label Text='<%# Eval("Educational") %>' runat="server" ID="EducationalLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("LeagueRosterID") %>' runat="server" ID="LeagueRosterIDLabel" visible="false" />
                        <asp:Label Text='<%# Eval("LeagueID") %>' runat="server" ID="LeagueIDLabel" visible="false" />
                        <asp:Label Text='<%# Eval("PresidentID") %>' runat="server" ID="PresidentIDLabel" visible="false"/>
                        <asp:Label Text='<%# Eval("UserAccountID") %>' runat="server" ID="UserAccountIDLabel" visible="false"/>
                        <asp:Label Text='<%# Eval("FirstName") %>' runat="server" ID="FirstNameLabel" visible="false"/>
                        <asp:Label Text='<%# Eval("LastName") %>' runat="server" ID="LastNameLabel" visible="false"/>
                        <asp:Label Text='<%# Eval("Email") %>' runat="server" ID="EmailLabel" visible="false" />
                        <asp:Label Text='<%# Eval("PresidentID1") %>' runat="server" ID="PresidentID1Label" visible="false" />
                        <asp:Label Text='<%# Eval("AVAILABLE") %>' runat="server" ID="AVAILABLELabel" visible="false" />
                        <asp:Label Text='<%# Eval("TEAMID") %>' runat="server" ID="TEAMIDLabel" visible="false" />
                        <asp:Label Text='<%# Eval("PresidentNum") %>' runat="server" ID="PresidentNumLabel" visible="false" />
                    </td>
                </tr>
            </ItemTemplate>
            <LayoutTemplate>
                <table runat="server" border="0">
                    <tr runat="server">
                        <td runat="server">
                            <table runat="server" id="itemPlaceholderContainer" style="" border="0">
                                <tr runat="server" style="">
<%--                                    <th runat="server">LeagueRosterID</th>
                                    <th runat="server">LeagueID</th>
                                    <th runat="server">PresidentID</th>
                                    <th runat="server">UserAccountID</th>
                                    <th runat="server">FirstName</th>
                                    <th runat="server">LastName</th>
                                    <th runat="server">Email</th>
                                        <th runat="server">PresidentNum</th>
                                    <th runat="server">PresidentID1</th>--%>
                                    <th runat="server">&nbsp</th>
                                    <th runat="server">&nbsp</th>
                                    <th runat="server">President</th>
                                    <th runat="server">Speech</th>
                                    <th runat="server">Campaigning</th>
                                    <th runat="server">Foreign</th>
                                    <th runat="server">Domestic</th>
                                    <th runat="server">Economic</th>
                                    <th runat="server">Military</th>
                                    <th runat="server">Legal</th>
                                    <th runat="server">Educational</th>
<%--                                    <th runat="server">AVAILABLE</th>
                                    <th runat="server">TEAMID</th>--%>
                                </tr>
                                <tr>
                                    <td colspan="11">
                                        <hr />
                                    </td>
                                </tr>
                                <tr runat="server" id="itemPlaceholder"></tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td runat="server" style=""></td>
                    </tr>
                </table>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label Text='<%# Eval("LeagueRosterID") %>' runat="server" ID="LeagueRosterIDLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("LeagueID") %>' runat="server" ID="LeagueIDLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("PresidentID") %>' runat="server" ID="PresidentIDLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("UserAccountID") %>' runat="server" ID="UserAccountIDLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("FirstName") %>' runat="server" ID="FirstNameLabel" visible="false" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("LastName") %>' runat="server" ID="LastNameLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("Email") %>' runat="server" ID="EmailLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("PresidentID1") %>' runat="server" ID="PresidentID1Label" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("PresidentName") %>' runat="server" ID="PresidentNameLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("PresidentNum") %>' runat="server" ID="PresidentNumLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("Speech") %>' runat="server" ID="SpeechLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("Campaigning") %>' runat="server" ID="CampaigningLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("Foreign") %>' runat="server" ID="ForeignLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("Domestic") %>' runat="server" ID="DomesticLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("Economic") %>' runat="server" ID="EconomicLabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("AVAILABLE") %>' runat="server" ID="AVAILABLELabel" /></td>
                    <td>
                        <asp:Label Text='<%# Eval("TEAMID") %>' runat="server" ID="TEAMIDLabel" /></td>
                    <td>
                </tr>
            </SelectedItemTemplate>
        </asp:ListView>                
                </td>
            </tr>
        </table>
     </asp:Panel>
    </div>
    </form>
</body>
</html>
