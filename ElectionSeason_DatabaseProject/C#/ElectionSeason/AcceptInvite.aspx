﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcceptInvite.aspx.cs" Inherits="ElectionSeason.AcceptInvite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/Login.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divLogin">
        <img src="logo.jpg" alt="logo" style="height:150px;width:300px;" /><br /><br />

        <h3>You accepted the following league invite:</h3>
        <asp:Label ID="lblAcceptInvite" runat="server" Text="" CssClass="label"></asp:Label><br /><br />
        <asp:Button ID="btnHome" runat="server" Text="Go To Home" /><br /><br />
        <asp:Label ID="lblWarning" runat="server" Text="Unauthorized access is prohibited by law" CssClass="label"></asp:Label><br />
        <asp:Label ID="lblCopyright" runat="server" Text="Copyright © ES Software" CssClass="label"></asp:Label>
    </div>
    </form>
</body>
</html>
