﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ElectionSeason.Utilities;

namespace ElectionSeason
{

    public partial class AccountManagement : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lv_AccountManagement_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {
            e.KeepInEditMode = true;
        }
       protected void lv_AccountManagement_ItemUpdating(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;

            Label lbl_UserAccountID = (Label)lv.Items[0].FindControl("lbl_UserAccountID");
            TextBox txt_FirstName = (TextBox)lv.Items[0].FindControl("txt_FirstName");
            TextBox txt_LastName = (TextBox)lv.Items[0].FindControl("txt_LastName");
            TextBox txt_PhoneNumber = (TextBox)lv.Items[0].FindControl("txt_PhoneNumber");
            Label lbl_Email = (Label)lv.Items[0].FindControl("lbl_Email");

            ds_AccountManagement.UpdateParameters["UserAccountID"].DefaultValue = lbl_UserAccountID.Text;
            ds_AccountManagement.UpdateParameters["FirstName"].DefaultValue = txt_FirstName.Text.ToString();
            ds_AccountManagement.UpdateParameters["LastName"].DefaultValue = txt_LastName.Text.ToString();
            ds_AccountManagement.UpdateParameters["PhoneNumber"].DefaultValue = txt_PhoneNumber.Text.ToString();

            try 
            {
	             ds_AccountManagement.Update();
                 string strException = "Update successful.";
                 MessageBox(strException);
                 string subject = "Election Season - Account has been modified";
                 string body = String.Format("There has been a modification applied to your account");
                 bool isSent = ESEMLUtility.SendEmail(lbl_Email.Text.ToString(), subject, body);

            } 
            catch (Exception ex) 
            {
                string strException = "There was a problem with the update.";
                MessageBox(strException); 
            }

        }


       protected void btn_Cancel(object sender, EventArgs e)
       {
           Response.Redirect("Home.aspx");
       }

       public void MessageBox(string message)
       {
           Label label = new Label();
           label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
           Page.Controls.Add(label);
       }
    }
}