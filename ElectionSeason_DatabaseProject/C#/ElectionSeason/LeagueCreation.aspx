﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeagueCreation.aspx.cs" Inherits="ElectionSeason.LeagueCreation"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="leagueCreation.css"/>
</head>
<body id="body">
    <form id="form1" runat="server">
    <div id="divLeagueCreation">
        <img src="logo.jpg" alt="logo" style="height:150px;width:300px;" /><br /><br />
        <asp:Label ID="lblCreateLeague" runat="server" Text="Create Your League" CssClass="label"></asp:Label><br /><br />
        <asp:Label ID="lblEnterName" runat="server" Text="Enter your league name:" CssClass="label2"></asp:Label>
        <asp:TextBox ID="tbLeagueName" runat="server" cssclass="textbox1"></asp:TextBox><br /><br />
        <asp:Label ID="lblEnterSize" runat="server" Text="Enter your league Size (4-8):" CssClass="label2"></asp:Label>
        <asp:TextBox ID="tbLeagueSize" runat="server" cssClass="textbox2"></asp:TextBox><br /><br />
        <asp:Button ID="btnCreateLeague" runat="server" Text="Create League" CssClass="button" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" /><br /><br />
        <asp:Label ID="lblWarning" runat="server" Text="Unauthorized access is prohibited by law" CssClass="label"></asp:Label><br />
        <asp:Label ID="lblCopyright" runat="server" Text="Copyright © ES Software" CssClass="label"></asp:Label>
    </div>
    </form>
</body>
</html>
