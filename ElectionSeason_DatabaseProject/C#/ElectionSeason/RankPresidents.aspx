﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RankPresidents.aspx.cs" Inherits="ElectionSeason.RankPresidents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <script type="text/javascript"> function showValue1(newValue) { document.getElementById("speech").innerHTML = newValue; } </script> 
    <script type="text/javascript"> function showValue2(newValue) { document.getElementById("campaign").innerHTML = newValue; } </script>
    <script type="text/javascript"> function showValue3(newValue) { document.getElementById("foreign").innerHTML = newValue; } </script>
    <script type="text/javascript"> function showValue4(newValue) { document.getElementById("domestic").innerHTML = newValue; } </script>
    <script type="text/javascript"> function showValue5(newValue) { document.getElementById("economic").innerHTML = newValue; } </script>
    <script type="text/javascript"> function showValue6(newValue) { document.getElementById("military").innerHTML = newValue; } </script>
    <script type="text/javascript"> function showValue7(newValue) { document.getElementById("legal").innerHTML = newValue; } </script>
    <script type="text/javascript"> function showValue8(newValue) { document.getElementById("education").innerHTML = newValue; } </script>

    <form id="form1" runat="server" method="post" style="margin: 0; text-align: center;">
    <div>
        <table>
            <tbody>   
                <tr class = 'no-border-row'>
                <td style="width: 80px"><asp:Label ID="Label20" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 120px"><asp:Label ID="Label1" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 180px"><asp:Label ID="lblPresName" runat="server" Text="George Washington"  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 120px"><asp:Label ID="Label2" runat="server" Text="" Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                </tr>   
                <tr class = 'no-border-row'>
                <td style="width: 80px"><asp:Label ID="Label21" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 120px"><asp:Button ID="btnMoveLeft" runat="server" Text="<"  CssClass="button" /></td>
                <td style="width: 180px"><asp:Image ID="imgPresident"  alt="logo" style="height:150px;width:150px;" runat="server" ></asp:Image></td>
                <td style="width: 120px"><asp:Button ID="btnMoveRight" runat="server" Text=">" cssClass="button" /></td>
                </tr>  
                <tr class = 'no-border-row'>
                <td style="width: 80px"><asp:Label ID="Label22" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 120px"><asp:Label ID="Label3" runat="server" Text="" Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 180px"><asp:Label ID="lblPresNum" runat="server" Text="1" Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 120px"><asp:Label ID="Label5" runat="server" Text="" Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                </tr>   
            </tbody>
        </table>​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
        <br /><br /><br />
        <table  id="category" runat="server">
            <tbody>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label4" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="lblSpeech" runat="server" Text="Speeches:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input type="range" name="speeches" id="speeches" runat="server" min="0" max="100" step="1" onchange="showValue1(this.value)" /></td>
                    <td style="width: 30px"><span id="speech" runat="server"></span></td>
                </tr>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label7" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="Label6" runat="server" Text="Campaigning Abilities:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input name="campaigning" type="range" id="campaigning" runat="server" min="0" max="100" step="1" onchange="showValue2(this.value)" /></td>
                    <td style="width: 30px"><span id="campaign" runat="server"></span></td>
                </tr>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label23" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="Label8" runat="server" Text="Foreign Affairs:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input name="foreignaffairs" type="range" id="foreignaffairs" runat="server" min="0" max="100" step="1" onchange="showValue3(this.value)" /></td>
                    <td style="width: 30px"><span id="foreign" runat="server"></span></td>
                </tr>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label24" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="Label10" runat="server" Text="Domestic Affairs:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input name="domesticaffairs" type="range" id="domesticaffairs" runat="server" min="0" max="100" step="1" onchange="showValue4(this.value)" /></td>
                    <td style="width: 30px"><span id="domestic" runat="server"></span></td>
                </tr>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label25" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="Label12" runat="server" Text="Economic Issues:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input name="economicissues" type="range" id="economicissues" runat="server" min="0" max="100" step="1" onchange="showValue5(this.value)" /></td>
                    <td style="width: 30px"><span id="economic" runat="server"></span></td>
                </tr>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label26" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="Label14" runat="server" Text="Military Affairs:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input name="militaryaffairs" type="range" id="militaryaffairs" runat="server" min="0" max="100" step="1" onchange="showValue6(this.value)" /></td>
                    <td style="width: 30px"><span id="military" runat="server"></span></td>
                </tr>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label27" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="Label16" runat="server" Text="Legal Issues:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input name="legalissues" type="range" id="legalissues" runat="server" min="0" max="100" step="1" onchange="showValue7(this.value)" /></td>
                    <td style="width: 30px"><span id="legal" runat="server"></span></td>
                </tr>
                <tr class ="no-border-row">
                    <td style="width: 35px"><asp:Label ID="Label28" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                    <td style="width: 210px"><asp:Label ID="Label18" runat="server" Text="Educational Issues:" Font-Bold="true" Font-Size= "14px" ></asp:Label></td>
                    <td style="width: 180px"><input name="educationissues" type="range" id="educationissues" runat="server" min="0" max="100" step="1" onchange="showValue8(this.value)" /></td>
                    <td style="width: 30px"><span id="education" runat="server"></span></td>
                </tr>
            </tbody>
        </table>
        <br /><br /><br />
        <table>
            <tbody>
                <tr class ="no-border-row">
                <td style="width: 50px"><asp:Label ID="Label9" runat="server" Text=""  Font-Bold="true" Font-Size= "18px" ></asp:Label></td>
                <td style="width: 110px"><asp:Button ID="btnRandomize" runat="server" Text="Randomize" CssClass="button" /></td>
                <td style="width: 110px"><asp:Button ID="btnRandomizeAll" runat="server" Text="Randomize All" CssClass="button" /></td>
                <td style="width: 110px"><asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" /></td>
                <td style="width: 110px"><asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" /></td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
