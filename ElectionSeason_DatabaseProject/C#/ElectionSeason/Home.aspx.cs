﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ElectionSeason.Utilities;


namespace ElectionSeason
{
    public partial class Home : System.Web.UI.Page
    {
        DataTable dt = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["UserAccountID"] = 1;
            if (Session["UserAccountID"] == null)
            {
                string strException = "Not authenticated. Please login again";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + strException + "');  window.location='" + Request.ApplicationPath + "Login.aspx';", true);
           }
        }
        protected override void OnInit(EventArgs e)
        {
            this.btnEnter.Click += new EventHandler(btnEnter_Click);
            SetDropDownList();
            base.OnInit(e);
        }
        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }
        private void SetDropDownList()
        {
            try
            {
                this.dt = ESDBUtility.GetScreens();
                ddlScreens.DataSource = dt;
                ddlScreens.DataTextField = dt.Columns[1].ToString();
                ddlScreens.DataValueField = dt.Columns[0].ToString();
                ddlScreens.DataBind();
            }
            catch
            {
                MessageBox("Unable to bind drop down list");
            }
        }
        protected void btnEnter_Click(object sender, EventArgs e)
        {
            if (dt.Rows.Count > 0)
            {
                int selectedValue = ddlScreens.SelectedIndex;
                Response.Redirect(dt.Rows[selectedValue][2].ToString());
            }


        }
    }
}