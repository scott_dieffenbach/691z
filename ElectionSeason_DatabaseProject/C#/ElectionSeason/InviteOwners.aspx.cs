﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ElectionSeason.Utilities;
using System.Text.RegularExpressions;

namespace ElectionSeason
{
    public partial class InviteOwners : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string leagueName = Request.QueryString["ln"];
            lblYourLeague.Text = leagueName;
            int leagueSize = Convert.ToInt16(Request.QueryString["ls"]);

            if (leagueSize >= 4)
            {
                for ( int i = leagueSize + 1; i < 9; i++)
                {
                    switch(i)
                    {
                        case 5: 
                            tbFirstName4.Enabled = false;
                            tbLastName4.Enabled = false;
                            tbEmailAddy4.Enabled = false;
                            break;
                        case 6:
                            tbFirstName5.Enabled = false;
                            tbLastName5.Enabled = false;
                            tbEmailAddy5.Enabled = false;
                            break;
                        case 7:
                            tbFirstName6.Enabled = false;
                            tbLastName6.Enabled = false;
                            tbEmailAddy6.Enabled = false;
                            break;
                        case 8:
                            tbFirstName7.Enabled = false;
                            tbLastName7.Enabled = false;
                            tbEmailAddy7.Enabled = false;
                            break;
                        default:
                            break;
                    }
                }
            }

        }
        protected override void OnInit(EventArgs e)
        {
            this.btnInviteOwner.Click += new EventHandler(btnInviteOwner_Click);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
            this.btnClear.Click += new EventHandler(btnClear_Click);
            base.OnInit(e);
        }
        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            tbFirstName1.Text = "";
            tbLastName1.Text = "";
            tbEmailAddy1.Text = "";
            tbFirstName2.Text = "";
            tbLastName2.Text = "";
            tbEmailAddy2.Text = "";
            tbFirstName3.Text = "";
            tbLastName3.Text = "";
            tbEmailAddy3.Text = "";
            tbFirstName4.Text = "";
            tbLastName4.Text = "";
            tbEmailAddy4.Text = "";
            tbFirstName5.Text = "";
            tbLastName5.Text = "";
            tbEmailAddy5.Text = "";
            tbFirstName6.Text = "";
            tbLastName6.Text = "";
            tbEmailAddy6.Text = "";
            tbFirstName7.Text = "";
            tbLastName7.Text = "";
            tbEmailAddy7.Text = "";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //Response.Redirect("./LeagueHome.aspx", true);
            int leagueSize = Convert.ToInt16(Request.QueryString["ls"]);
            int leagueID = Convert.ToInt16(Request.QueryString["lid"]);
            string leagueName = (Request.QueryString["ln"].ToString());
            Response.Redirect("./LeagueHome.aspx?ln=" + leagueName + "&ls=" + leagueSize + "&lid=" + leagueID + "", true);
        }

        protected void btnInviteOwner_Click(object sender, EventArgs e)
        {
            int leagueSize = Convert.ToInt16(Request.QueryString["ls"]);
            int leagueID = Convert.ToInt16(Request.QueryString["lid"]);
            int commish = Convert.ToInt16(Request.QueryString["Com"]);

            string firstName = "";
            string lastName = "";
            string emailAddy = "";
            try
            {
                for (int i = 1; i < leagueSize; i++)
                {
                    switch (i)
                    {
                        case 1:
                            firstName = tbFirstName1.Text;
                            lastName = tbLastName1.Text;
                            emailAddy = tbEmailAddy1.Text;
                            break;
                        case 2:
                            firstName = tbFirstName2.Text;
                            lastName = tbLastName2.Text;
                            emailAddy = tbEmailAddy2.Text;
                            break;
                        case 3:
                            firstName = tbFirstName3.Text;
                            lastName = tbLastName3.Text;
                            emailAddy = tbEmailAddy3.Text;
                            break;
                        case 4:
                            firstName = tbFirstName4.Text;
                            lastName = tbLastName4.Text;
                            emailAddy = tbEmailAddy4.Text;
                            break;
                        case 5:
                            firstName = tbFirstName5.Text;
                            lastName = tbLastName5.Text;
                            emailAddy = tbEmailAddy5.Text;
                            break;
                        case 6:
                            firstName = tbFirstName6.Text;
                            lastName = tbLastName6.Text;
                            emailAddy = tbEmailAddy6.Text;
                            break;
                        case 7:
                            firstName = tbFirstName7.Text;
                            lastName = tbLastName7.Text;
                            emailAddy = tbEmailAddy7.Text;
                            break;
                        default:
                            break;
                    }

                    bool validated = ValidateInput(firstName, lastName, emailAddy);
                    bool emailExist = false;
                    string userID = "";
                    char invite = 'N';
                    bool isUpdatedLeagueAssoc = false;

                    int numberofSameEmail = ESDBUtility.CheckForSameEmail(emailAddy);
                    if (numberofSameEmail == -1)
                    {
                        MessageBox("Internal Error, please contact application support");
                    }
                    else if (numberofSameEmail > 0)
                    {
                        emailExist = true;
                    }
                    else if (validated && !emailExist)
                    {
                        bool isUpdated = ESDBUtility.CreateInvitedUser(firstName, lastName, emailAddy);
                        userID = Convert.ToString(ESDBUtility.GetUserAccountID(emailAddy));
                        isUpdatedLeagueAssoc = ESDBUtility.CreateLeagueAssoc(leagueID, userID, invite);

                        if (!isUpdated)
                        {
                            MessageBox("Create Invited User Failed");
                        }
                        else
                        {
                            SendEmail(emailAddy, leagueID, i);
                        }
                    }
                    else if (validated && emailExist)
                    {
                        userID = Convert.ToString(ESDBUtility.GetUserAccountID(emailAddy));
                        isUpdatedLeagueAssoc = ESDBUtility.CreateLeagueAssoc(leagueID, userID, invite);
                        SendEmail(emailAddy, leagueID, i);
                    }
                    if (validated && isUpdatedLeagueAssoc)
                    {
                        MessageBox("Owner " + i + " creation complete");
                    }
                    else if (!validated)
                    {
                        MessageBox("Unable to create owner " + i + ". Each owner must have a valid first and last name, and email address");
                    }
                    else
                    {
                        MessageBox("Invite Owner failed due to database unable to update LeagueAssoc");
                    }
                }

                Response.Redirect("./RankPresidents.aspx?lid=" + leagueID + "", true);
            }
            catch
            {
                MessageBox("It Failed in btnInviteOwner");
            }
        }

        protected void SendEmail(string emailAddy, int leagueID, int loop)
        {
                    string emailSubject = "Please Join your Election Season League";  
                    string emailBody = "test test test";

                    try
                    {
                        bool emailSent = ESEMLUtility.SendEmail(emailAddy, emailSubject, emailBody);
                        if (emailSent)
                        {
                            MessageBox("Invite Owner " + loop + " Complete");
                        }
                        else if (!emailSent)
                            {
                                MessageBox("Unable to email user " + loop + ".");
                            }
                    }
                    catch
                    {
                        MessageBox("Failure in SendEmail");
                    }
        }

        protected static bool ValidateInput(string firstName, string lastName, string emailAddy)
        {
            bool isValidated = true;

            if (String.IsNullOrWhiteSpace(firstName))
            {
                isValidated = false;
            }
            if (String.IsNullOrWhiteSpace(lastName))
            {
                isValidated = false;
            }
            if (String.IsNullOrWhiteSpace(emailAddy))
            {
                isValidated = false;
            }
            else 
            {
                bool goodEmail = TestEmail.IsEmail(emailAddy);
                if (!goodEmail)
                {
                    isValidated = false;
                }
            }
            return(isValidated);
        }

        public static class TestEmail
        {
            /// <summary>
            /// Regular expression, which is used to validate an E-Mail address.
            /// </summary>
            public const string MatchEmailPattern =
                      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
               + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
               + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
               + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

            /// <summary>
            /// Checks whether the given Email-Parameter is a valid E-Mail address.
            /// </summary>
            /// <param name="email">Parameter-string that contains an E-Mail address.</param>
            /// <returns>True, when Parameter-string is not null and 
            /// contains a valid E-Mail address;
            /// otherwise false.</returns>
            public static bool IsEmail(string email)
            {
                if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
                else return false;
            }
        }        
    }
}