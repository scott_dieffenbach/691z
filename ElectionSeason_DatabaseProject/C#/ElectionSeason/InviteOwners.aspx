﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InviteOwners.aspx.cs" Inherits="ElectionSeason.InviteOwners" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="InviteOwners.css"/>
</head>
<body id="body">
    <form id="form1" runat="server">
    <div id="divInviteOwners">
        <img src="logo.jpg" alt="logo" style="height:150px;width:300px;" /><br /><br />
        <asp:Label ID="lblCreateLeague" runat="server" Text="Invite Owners To " CssClass="label"></asp:Label>
        <asp:Label ID="lblYourLeague" runat="server" Text="Your League" CssClass="label"></asp:Label>
        <br /><br />
        <table cellpadding="1" cellspacing="1" height="125" width="600">
            <tbody>     
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:Label ID="lblFirstName" runat="server" Text="First Name" cssclass="label2"></asp:Label></td>
                <td style="width: 135px"><asp:Label ID="lblLastName" runat="server" Text="Last Name" cssClass="label2"></asp:Label></td>
                <td style="width: 200px"><asp:Label ID="lblEmailAddy" runat="server" Text="Email Address" cssClass="label2"></asp:Label></td>
                </tr>  
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:TextBox ID="tbFirstName1" runat="server" cssclass="textbox1"></asp:TextBox></td>
                <td style="width: 135px"><asp:TextBox ID="tbLastName1" runat="server" cssClass="textbox1"></asp:TextBox></td>
                <td style="width: 200px"><asp:TextBox ID="tbEmailAddy1" runat="server" cssClass="textbox2"></asp:TextBox></td>
                </tr>
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:TextBox ID="tbFirstName2" runat="server" cssclass="textbox1"></asp:TextBox></td>
                <td style="width: 135px"><asp:TextBox ID="tbLastName2" runat="server" cssClass="textbox1"></asp:TextBox></td>
                <td style="width: 200px"><asp:TextBox ID="tbEmailAddy2" runat="server" cssClass="textbox2"></asp:TextBox></td>
                </tr>
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:TextBox ID="tbFirstName3" runat="server" cssclass="textbox1"></asp:TextBox></td>
                <td style="width: 135px"><asp:TextBox ID="tbLastName3" runat="server" cssClass="textbox1"></asp:TextBox></td>
                <td style="width: 200px"><asp:TextBox ID="tbEmailAddy3" runat="server" cssClass="textbox2"></asp:TextBox></td>
                </tr>
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:TextBox ID="tbFirstName4" runat="server" cssclass="textbox1"></asp:TextBox></td>
                <td style="width: 135px"><asp:TextBox ID="tbLastName4" runat="server" cssClass="textbox1"></asp:TextBox></td>
                <td style="width: 200px"><asp:TextBox ID="tbEmailAddy4" runat="server" cssClass="textbox2"></asp:TextBox></td>
                </tr>                
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:TextBox ID="tbFirstName5" runat="server" cssclass="textbox1"></asp:TextBox></td>
                <td style="width: 135px"><asp:TextBox ID="tbLastName5" runat="server" cssClass="textbox1"></asp:TextBox></td>
                <td style="width: 200px"><asp:TextBox ID="tbEmailAddy5" runat="server" cssClass="textbox2"></asp:TextBox></td>
                </tr>
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:TextBox ID="tbFirstName6" runat="server" cssclass="textbox1"></asp:TextBox></td>
                <td style="width: 135px"><asp:TextBox ID="tbLastName6" runat="server" cssClass="textbox1"></asp:TextBox></td>
                <td style="width: 200px"><asp:TextBox ID="tbEmailAddy6" runat="server" cssClass="textbox2"></asp:TextBox></td>
                </tr>                
                <tr class = 'no-border-row'>
                <td style="width: 135px"><asp:TextBox ID="tbFirstName7" runat="server" cssclass="textbox1"></asp:TextBox></td>
                <td style="width: 135px"><asp:TextBox ID="tbLastName7" runat="server" cssClass="textbox1"></asp:TextBox></td>
                <td style="width: 200px"><asp:TextBox ID="tbEmailAddy7" runat="server" cssClass="textbox2"></asp:TextBox></td>
                </tr>            
            </tbody>
        </table>​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
        <br /><br />
        <asp:Button ID="btnInviteOwner" runat="server" Text="Invite Owners" CssClass="button" />
        <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" /><br /><br />
        <asp:Label ID="lblWarning" runat="server" Text="Unauthorized access is prohibited by law" CssClass="label"></asp:Label><br />
        <asp:Label ID="lblCopyright" runat="server" Text="Copyright © ES Software" CssClass="label"></asp:Label>
    </div>
    </form>
</body>
</html>

