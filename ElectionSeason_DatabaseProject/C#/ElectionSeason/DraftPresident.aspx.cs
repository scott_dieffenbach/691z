﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
/// Will need to pass the LeagueID and the UserAccount ID from the previous page for this to work. 

namespace ElectionSeason
{
    public partial class DraftPresident : System.Web.UI.Page
    {
        Boolean IsDraftEmpty = true;
        Int64 UserAccountID = 0;
        Int64 LeagueID = 0; //Convert.ToInt64(HttpContext.Current.Session["LeagueID"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource ds_Random = ds_CreateRandomizedDraft;
            SqlDataSource ds_GetLeague = ds_GetLeagueIDByUserID;

            //HttpContext.Current.Session["UserAccountID"] = 1;
            if (HttpContext.Current.Session["UserAccountID"] == null)
            {
                string strException = "Not authenticated. Please login again";
                MessageBox(strException);
                Response.Redirect("Login.aspx");
            }
            else
            {
                UserAccountID = (Convert.ToInt64(HttpContext.Current.Session["UserAccountID"]));
            }
            LeagueID = Convert.ToInt64(Request.QueryString["lid"]);

            if (gv_ManageDraft.Rows.Count < 1)
            {
                ds_Random.SelectParameters["LeagueID"].DefaultValue = LeagueID.ToString(); //***********************************
                ds_Random.Select(DataSourceSelectArguments.Empty);

            }
        }

        protected void gv_ShowAvailableRoster_OnInit(object sender, EventArgs e)
        {
            SqlDataSource ds_ChosenDraftRoster = ds_ShowChosenDraftRoster;
            ds_ChosenDraftRoster.SelectParameters["LeagueID"].DefaultValue = LeagueID.ToString();
            ds_ChosenDraftRoster.SelectParameters["UserAccountID"].DefaultValue = UserAccountID.ToString();
            ds_ChosenDraftRoster.DataBind();
            //ds_ChosenDraftRoster.Select(DataSourceSelectArguments.Empty);
        }
        protected void gv_ShowAvailableDraftPicks_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
             SqlDataSource ds_ChosenPicks = ds_ShowChosenDraftRoster;
             SqlDataSource ds_AvailablePicks = ds_ShowAvailableDraftPicks;
             SqlDataSource ds_MgDraft = ds_ManageDraft;
             int priority = 0; 

             DataView dv = (DataView)ds_ManageDraft.Select(DataSourceSelectArguments.Empty);
             DataTable dt = dv.ToTable();
             int index = 0;

             if (e.CommandName == "Draft")
             {
                 foreach (DataRow row in dt.Rows)
                 {
                     String LeagueDraftID = row["LeagueDraftID"].ToString();
                     String draftPriority = row["DraftPriority"].ToString();
                     if ((row["UserAccountID"].ToString() == UserAccountID.ToString()) && (row["IsItMyTurn"].ToString() == "True"))
                     {
                         //Update IsItMyTurn to False/0
                         ds_MgDraft.UpdateParameters["IsItMyTurn"].DefaultValue = "false";
                         ds_MgDraft.UpdateParameters["DraftPriority"].DefaultValue = draftPriority.ToString();
                         ds_MgDraft.UpdateParameters["LeagueDraftID"].DefaultValue = LeagueDraftID.ToString();
                         ds_MgDraft.Update();

                         Int32 rowIndex = Convert.ToInt32(e.CommandArgument);
                         //Get League ID
                         Int64 LeagueID = Convert.ToInt32(gv_ShowAvailableDraftPicks.DataKeys[rowIndex].Values[0]);
                         //Get President ID
                         Int64 PresidentID = Convert.ToInt32(gv_ShowAvailableDraftPicks.DataKeys[rowIndex].Values[1]);

                         //ds.InsertParameters["PresidentID"].DefaultValue = gv_ShowAvailableDraftPicks.DataKeys[key]["PresidentID"].ToString();	
                         ds_ChosenPicks.InsertParameters["LeagueID"].DefaultValue = LeagueID.ToString();
                         ds_ChosenPicks.InsertParameters["PresidentID"].DefaultValue = PresidentID.ToString();
                         ds_ChosenPicks.InsertParameters["UserAccountID"].DefaultValue = UserAccountID.ToString();
                         ds_ChosenPicks.Insert();


                         ds_AvailablePicks.SelectParameters["LeagueID"].DefaultValue = LeagueID.ToString();
                         ds_AvailablePicks.SelectParameters["UserAccountID"].DefaultValue = UserAccountID.ToString();
                         ds_AvailablePicks.Select(DataSourceSelectArguments.Empty);
                         gv_ShowAvailableDraftPicks.DataBind();

                         if ((index + 1) == dt.Rows.Count)
                         {
                            priority = 1;
                         }
                         else
                         {
                             priority = Convert.ToInt32(draftPriority.ToString()) + 1;
                         }
                     }
                     index++; 
                 }

                 foreach (DataRow row in dt.Rows)
                 {
                     String LeagueDraftID = row["LeagueDraftID"].ToString();
                     String draftPriority = row["DraftPriority"].ToString();
                     if ((row["DraftPriority"].ToString() == priority.ToString())  && (row["IsItMyTurn"].ToString() == "False"))
                     {
                         if ((index + 1) == dt.Rows.Count)
                         {
                             //Update where draft priority = 1 
                             ds_MgDraft.UpdateParameters["IsItMyTurn"].DefaultValue = "true";
                             ds_MgDraft.UpdateParameters["DraftPriority"].DefaultValue = draftPriority.ToString();
                             ds_MgDraft.UpdateParameters["LeagueDraftID"].DefaultValue = LeagueDraftID.ToString();
                             ds_MgDraft.Update();
                         }
                         else
                         {
                             ds_MgDraft.UpdateParameters["IsItMyTurn"].DefaultValue = "true";
                             ds_MgDraft.UpdateParameters["DraftPriority"].DefaultValue = draftPriority.ToString();
                             ds_MgDraft.UpdateParameters["LeagueDraftID"].DefaultValue = LeagueDraftID.ToString();
                             ds_MgDraft.Update();
                         }
                     }
                     index++; 
                 }
            }
        }

        protected void gv_ManageDraft_OnInit(object sender, EventArgs e)
        {
            SqlDataSource ds_GetLeague = ds_GetLeagueIDByUserID;
            ds_ManageDraft.SelectParameters["LeagueID"].DefaultValue = LeagueID.ToString();
        }

        protected void gv_ShowAvailableDraftPicks_OnLoad(object sender, EventArgs e)
        {
            SqlDataSource ds_AvailableDraftPicks = ds_ShowAvailableDraftPicks;
            ds_AvailableDraftPicks.SelectParameters["LeagueID"].DefaultValue = LeagueID.ToString();
            ds_AvailableDraftPicks.SelectParameters["UserAccountID"].DefaultValue = UserAccountID.ToString();
            //gv_ShowAvailableDraftPicks.DataBind();
        }
        
        protected void btn_BeginDraft_OnClick(object sender, EventArgs e)
        {
            if (IsDraftEmpty == true)
            {
                IsDraftEmpty = false;
            }
        }

        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }

        protected void Lnk_ReturnToLeagueHome_OnClick(object sender, EventArgs e)
        {
            int leagueID = Convert.ToInt16(Request.QueryString["lid"]);
            Response.Redirect("./LeagueHome.aspx?lid=" + leagueID + "", true);
        }
    }
}