﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ElectionSeason.Utilities;

namespace ElectionSeason
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnInit(EventArgs e)
        {
            this.btnLogin.Click += new EventHandler(btnLogin_Click);
            this.lbForgotPassword.Click += new EventHandler(lbForgotPassword_Click);
            base.OnInit(e);
        }
        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string userName = tbUsername.Text;
            string password = tbPassword.Text;
            string idNumber = ESDBUtility.IsUser(userName, password);
            if (idNumber != "")
            {
                Session["UserAccountID"] = idNumber;
                tbUsername.Text = "";
                tbPassword.Text = "";
                Response.Redirect("Home.aspx");
            }
            else
            {
                MessageBox("Incorrect username and password. Try again.");
                tbUsername.Text = "";
                tbPassword.Text = "";
            }
        }
        protected void lbForgotPassword_Click(object sender, EventArgs e)
        {
            if(tbUsername.Text == null || tbUsername.Text == "")
            {
                MessageBox("Please enter user name.");
            }
            else
            {
                string emailAdress = ESDBUtility.GetEmailAddress(tbUsername.Text);
                if(emailAdress == null || emailAdress == "")
                {
                    MessageBox("Could not find an email address.");
                }
                else
                {
                    string userName = tbUsername.Text;
                    string password = "ElectionSeasion4";
                    bool isUpdated = ESDBUtility.UpdatePassword(userName, password);
                    if(isUpdated)
                    {
                        string subject = "Election Season Reset Password";
                        string body = String.Format("You password has been reset to {0}",password);
                        bool isSent = ESEMLUtility.SendEmail(emailAdress, subject, body);
                        if(isSent)
                        {
                            MessageBox("Email sent!");
                        }
                        else
                        {
                            MessageBox("Email could not be sent.");
                        }
                    }
                    else
                    {
                        MessageBox("Password could not be updated.");
                    }

                }
            }
                
        }
    }
}