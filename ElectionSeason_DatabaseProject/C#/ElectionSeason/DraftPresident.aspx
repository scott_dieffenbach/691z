﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DraftPresident.aspx.cs" Inherits="ElectionSeason.DraftPresident" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Draft Presidents</title>
    <link rel="stylesheet" type="text/css" href="css/ElectionSeason.css" />
</head>
<body>
    <form id="frm_DraftPresident" runat="server">
    <div>
       <img src="logo.jpg" alt="logo" style="height:150px;width:350px;" />
        <asp:SqlDataSource ID="ds_ShowAvailableDraftPicks" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>' OnLoad="gv_ShowAvailableDraftPicks_OnLoad"
            SelectCommand="SELECT li.[LeagueID],li.[LeagueName],li.[LeagueSize],li.[LeagueCommish],la.[UserAccountID],
                la.[InviteAccepted],p.[PresidentID],p.[PresidentName],p.[PresidentNum],pr.[Speech],pr.[Campaigning],
                pr.[Foreign],pr.[Domestic],pr.[Economic],pr.[Military],pr.[Legal], pr.[Educational],pr.[AVAILABLE],pr.[TEAMID],
				(pr.[Speech] + pr.[Campaigning] + pr.[Foreign] + pr.[Domestic] + pr.[Economic] + pr.[Military] + pr.[Legal] + pr.[Educational]) AS TotalRanking
            FROM 
	            [ElectionSeason_DB].[dbo].[LeagueInfo] li
	            JOIN [ElectionSeason_DB].[dbo].[LeagueAssoc] la ON la.[LeagueID] = li.[LeagueID]
	            JOIN [ElectionSeason_DB].[dbo].[PresidentRank] pr ON pr.[LeagueID] = li.[LeagueID] 
	            JOIN [ElectionSeason_DB].[dbo].[Presidents] p ON pr.[PresidentID] = p.[PresidentID] 
            WHERE
                la.[UserAccountID] = @UserAccountID
	            AND 
	            la.[LeagueID] = @LeagueID
				AND
				NOT EXISTS 
					(SELECT [PresidentID] FROM [dbo].[LeagueRoster] 
                        WHERE [LeagueID] = @LeagueID AND p.[PresidentID] = [PresidentID])
				ORDER BY TotalRanking DESC "> 
            <SelectParameters>
                <asp:Parameter Name="UserAccountID" DbType="Int64" ></asp:Parameter>
                <asp:Parameter Name="LeagueID" DbType="Int64" ></asp:Parameter>
            </SelectParameters>                    
        </asp:SqlDataSource>  
        
        <asp:SqlDataSource ID="ds_ShowChosenDraftRoster" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>' 
            SelectCommand="SELECT lr.[LeagueRosterID],lr.[LeagueID],lr.[PresidentID],lr.[UserAccountID],
                p.[PresidentName],p.[PresidentNum]
            FROM 
	            [ElectionSeason_DB].[dbo].[LeagueRoster] lr
	            JOIN [ElectionSeason_DB].[dbo].[Presidents] p ON lr.[PresidentID] = p.[PresidentID] 
            WHERE 	
	            lr.[UserAccountID] = @UserAccountID
	            AND 
	            lr.[LeagueID] = @LeagueID
            ORDER BY [PresidentNum]" 
            InsertCommand="INSERT INTO [dbo].[LeagueRoster]
                           ([LeagueID], [PresidentID], [UserAccountID])
                            VALUES
                           (@LeagueID, @PresidentID, @UserAccountID)">  
            <SelectParameters>
                <asp:Parameter Name="UserAccountID" DbType="Int64" ></asp:Parameter>
                <asp:Parameter Name="LeagueID" DbType="Int64" ></asp:Parameter>
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="LeagueID" DbType="Int64"></asp:Parameter>
                <asp:Parameter Name="PresidentID" DbType="Int64"></asp:Parameter>
                <asp:Parameter Name="UserAccountID" DbType="Int64"></asp:Parameter>
            </InsertParameters>                                             
        </asp:SqlDataSource>     
        
        <asp:SqlDataSource ID="ds_ManageDraft" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>' 
            SelectCommand="SELECT ld.[LeagueID],ld.[UserAccountID],ai.FirstName,ai.LastName,ai.Email,ld.[LeagueDraftID],
	                        ld.[DraftPriority],[IsitMyTurn]
                                FROM [ElectionSeason_DB].[dbo].[LeagueDraft] ld
					            JOIN [ElectionSeason_DB].[dbo].[AccountInformation] ai ON ai.[UserAccountID] = ld.[UserAccountID]
                            WHERE ld.LeagueID = @LeagueID ORDER BY ld.[DraftPriority]"
               UpdateCommand="UPDATE [dbo].[LeagueDraft] SET [IsitMyTurn] = @IsitMyTurn
                                WHERE LeagueDraftID = @LeagueDraftID and DraftPriority = @DraftPriority">     
            <SelectParameters>
                <asp:Parameter Name="LeagueID" DbType="Int64"></asp:Parameter>
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="LeagueDraftID" DbType="Int64"></asp:Parameter>
                <asp:Parameter Name="DraftPriority" DbType="Int64"></asp:Parameter>
                <asp:Parameter Name="IsitMyTurn" DbType="Boolean"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>       
        
        <asp:SqlDataSource ID="ds_CreateRandomizedDraft" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>' SelectCommand="sp_CreateRamdomizedDraft" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="LeagueID" DbType="Int64"></asp:Parameter>
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="ds_GetLeagueIDByUserID" runat="server" ConnectionString='<%$ ConnectionStrings:ES_DB_Connection %>' 
            SelectCommand="SELECT [LeagueID]  FROM [ElectionSeason_DB].[dbo].[LeagueAssoc] WHERE [UserAccountID] = @UserAccountID">
            <SelectParameters>
                <asp:Parameter Name="UserAccountID" DbType="Int64"></asp:Parameter>
            </SelectParameters>
        </asp:SqlDataSource>

        <table border="0">
            <tr>
                <td>
                    <asp:LinkButton ID="Lnk_ReturnToLeagueHome" runat="server" OnClick="Lnk_ReturnToLeagueHome_OnClick" Text="Return to League Home"></asp:LinkButton><br />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align: top; text-align: center">
<%--                    <asp:Button ID="btn_BeginDraft" runat="server" Text="Begin Draft" OnClick="btn_BeginDraft_OnClick"/>
                    <asp:Button ID="btn_CreateDraftPriority" runat="server" Text="Draft Order" />--%>
                    <asp:GridView ID="gv_ManageDraft" runat="server" DataSourceID="ds_ManageDraft" AutoGenerateColumns="False" OnLoad="gv_ManageDraft_OnInit" DataKeyNames="LeagueDraftID">
                        <Columns>
                            <asp:BoundField DataField="LeagueID" HeaderText="LeagueID" SortExpression="LeagueID" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="UserAccountID" HeaderText="UserAccountID" SortExpression="UserAccountID" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"></asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName"></asp:BoundField>
                            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email"></asp:BoundField>
                            <asp:BoundField DataField="LeagueDraftID" HeaderText="LeagueDraftID" SortExpression="LeagueDraftID" InsertVisible="False" ReadOnly="True" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="DraftPriority" HeaderText="Draft Order" SortExpression="DraftPriority"></asp:BoundField>
                            <asp:CheckBoxField DataField="IsitMyTurn" HeaderText="Who's Turn?" SortExpression="IsitMyTurn"></asp:CheckBoxField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                   <asp:Label runat="server" ID="lbl_ShowAvailableDraftPick" Text="Available Presidents" Font-Bold="True" Font-Size="Medium"></asp:Label>
                   <asp:GridView ID="gv_ShowAvailableDraftPicks" runat="server" DataSourceID="ds_ShowAvailableDraftPicks" OnLoad="gv_ShowAvailableDraftPicks_OnLoad"
                    DataKeyNames="LeagueID,PresidentID" OnRowCommand="gv_ShowAvailableDraftPicks_OnRowCommand" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="LeagueName" HeaderText="LeagueName" SortExpression="LeagueName" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="LeagueID" HeaderText="LeagueID" ReadOnly="True" InsertVisible="False" SortExpression="LeagueID" Visible="false" ></asp:BoundField>
                        <asp:BoundField DataField="LeagueName" HeaderText="LeagueName" SortExpression="LeagueName" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="LeagueSize" HeaderText="LeagueSize" SortExpression="LeagueSize" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="LeagueCommish" HeaderText="LeagueCommish" SortExpression="LeagueCommish" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="UserAccountID" HeaderText="UserAccountID" SortExpression="UserAccountID" Visible="false"></asp:BoundField>
                        <asp:TemplateField>
                            <Itemtemplate>
                                <asp:Button ID="btn_DraftPresident" runat="server" CommandName="Draft" Text="Draft"
                                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>
                            </Itemtemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <Itemtemplate>
                                <asp:Image ID="img_PresidentImage" ImageUrl='<%# string.Format("~/images/{0}.jpg", Eval("PresidentID")) %>' runat="server"  Width="40px" Height="40px" />                                 
                            </Itemtemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="InviteAccepted" HeaderText="InviteAccepted" SortExpression="InviteAccepted" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="PresidentID" HeaderText="PresidentID" ReadOnly="True" SortExpression="PresidentID" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="PresidentName" HeaderText="President" SortExpression="PresidentName"></asp:BoundField>
                        <asp:BoundField DataField="PresidentNum" HeaderText="PresidentNum" SortExpression="PresidentNum" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="Speech" HeaderText="Speech" SortExpression="Speech" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Campaigning" HeaderText="Campaign" SortExpression="Campaigning" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Foreign" HeaderText="Foreign" SortExpression="Foreign" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Domestic" HeaderText="Domestic" SortExpression="Domestic" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Economic" HeaderText="Economic" SortExpression="Economic" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Military" HeaderText="Military" SortExpression="Military" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Legal" HeaderText="Legal" SortExpression="Legal" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="Educational" HeaderText="Education" SortExpression="Educational" Visible="true" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="AVAILABLE" HeaderText="AVAILABLE" SortExpression="AVAILABLE" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="TEAMID" HeaderText="TEAMID" SortExpression="TEAMID" Visible="false"></asp:BoundField>
                        <asp:BoundField DataField="TotalRanking" HeaderText="Ranking" SortExpression="TotalRanking" Visible="True" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="NotSet" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                    </Columns>
                </asp:GridView>
                </td>
                <td>
                    &nbsp
                </td>
                <td style="vertical-align: top">
                   <asp:Label runat="server" ID="lbl_ShowAvailableRoster" Text="Current Roster" Font-Bold="True" Font-Size="Medium"></asp:Label>

                    <asp:GridView ID="gv_ShowAvailableRoster" runat="server" DataSourceID="ds_ShowChosenDraftRoster" AutoGenerateColumns="False" EmptyDataText="No draft picks selected yet."
                        DataKeyNames="LeagueRosterID,PresidentID" OnLoad="gv_ShowAvailableRoster_OnInit" >
                        <Columns>
                            <asp:BoundField DataField="LeagueRosterID" HeaderText="LeagueRosterID" ReadOnly="True" InsertVisible="False" SortExpression="LeagueRosterID" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="LeagueID" HeaderText="LeagueID" SortExpression="LeagueID" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="PresidentID" HeaderText="PresidentID" SortExpression="PresidentID" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="UserAccountID" HeaderText="UserAccountID" SortExpression="UserAccountID" Visible="false"></asp:BoundField>
                            <asp:TemplateField>
                                <Itemtemplate>
                                    <asp:Image ID="img_PresidentImage" ImageUrl='<%# string.Format("~/images/{0}.jpg", Eval("PresidentID")) %>' runat="server"  Width="40px" Height="40px" />                                 
                                </Itemtemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PresidentName" HeaderText="PresidentName" SortExpression="PresidentName"></asp:BoundField>
                            <asp:BoundField DataField="PresidentNum" HeaderText="PresidentNum" SortExpression="PresidentNum" Visible="false"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>




    </div>
    </form>
</body>
</html>
