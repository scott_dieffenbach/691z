﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ElectionSeason.Utilities;

namespace ElectionSeason_Web
{
    public partial class AccountInformation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_CreateAccount_Click(object sender, EventArgs e)
        {
            SqlDataSource CreateUserAccount = ds_CreateUserAccount;
            CreateUserAccount.SelectParameters["UserName"].DefaultValue = txt_Email.Text.ToString();
            DataView dv = (DataView)CreateUserAccount.Select(DataSourceSelectArguments.Empty);

            if (dv.Count == 0)
            {
                CreateUserAccount.InsertParameters["UserName"].DefaultValue = txt_Email.Text.ToString();
                CreateUserAccount.InsertParameters["UserPassword"].DefaultValue = txt_Password.Text.ToString();
                //CreateUserAccount.InsertParameters["LockedOut"].DefaultValue = "false";
                CreateUserAccount.Insert();

                string subject = "Election Season - Account Created";
                string body = String.Format("Your account has been created");
                bool isSent = ESEMLUtility.SendEmail(txt_Email.Text.ToString(), subject, body);
                MessageBox("User account created");

            }
            else if (dv.Count > 0)
            {
                string strException = "This UserName already exists. Please choose another. ";
                MessageBox(strException);               
            }                  
        }
        public void MessageBox(string message)
        {
            Label label = new Label();
            label.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + message + "')</script>";
            Page.Controls.Add(label);
        }


    }
}