﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeagueHome.aspx.cs" Inherits="ElectionSeason.LeagueHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/LeagueCreation.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divLeagueCreation">
        <img src="logo.jpg" alt="logo" style="height:150px;width:300px;" /><br /><br />
        <asp:Label ID="Label1" runat="server" Text="League Home" CssClass="label"></asp:Label><br /><br />
        <asp:Label ID="Label2" runat="server" Text="Please select an action from the dropdowns below:" CssClass="label"></asp:Label><br /><br />
        <asp:Label ID="lblYourLeagues" runat="server" Text="Go to Your League:" CssClass="label"></asp:Label>
        <asp:DropDownList ID="ddlYourLeagues" CssClass="textbox" runat="server"></asp:DropDownList><br /><br />
        <asp:Label ID="lblAcceptInvite" runat="server" Text="Accept Invite:" CssClass="label"></asp:Label>
        <asp:DropDownList ID="ddlAcceptInvite" CssClass="textbox" runat="server"></asp:DropDownList><br /><br />
        <asp:Label ID="lblCommish" runat="server" Text="League Config:" CssClass="label"></asp:Label>
        <asp:DropDownList ID="ddlLeagueConfig" CssClass="textbox" runat="server"></asp:DropDownList><br /><br />
        <asp:Button ID="btnCreateLeague" runat="server" Text="Create League" CssClass="button" /><br /><br />
        <asp:Label ID="lblWarning" runat="server" Text="Unauthorized access is prohibited by law" CssClass="label"></asp:Label><br />
        <asp:Label ID="lblCopyright" runat="server" Text="Copyright © ES Software" CssClass="label"></asp:Label>
    </div>
    </form>
</body>
</html>
