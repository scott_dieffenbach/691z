﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeagueConfig.aspx.cs" Inherits="ElectionSeason.LeagueConfig" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/Login.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divLogin">
        <img src="logo.jpg" alt="logo" style="height:150px;width:300px;" />
        <h3>League Configuration Manager</h3>
        <h6>Please select screen from drop down below</h6>
        <asp:DropDownList ID="ddlScreens" CssClass="textbox" runat="server"></asp:DropDownList><br /><br />
        <asp:Button ID="btnEnter" runat="server" Text="Enter" /><br /><br />
        <asp:Label ID="lblWarning" runat="server" Text="Unauthorized access is prohibited by law" CssClass="label"></asp:Label><br />
        <asp:Label ID="lblCopyright" runat="server" Text="Copyright © ES Software" CssClass="label"></asp:Label>
    </div>
    </form>
</body>
</html>
