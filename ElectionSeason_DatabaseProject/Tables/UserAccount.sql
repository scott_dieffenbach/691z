﻿CREATE TABLE [dbo].[UserAccess]
(
	[UserAccountID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [UserName] NVARCHAR(100) NULL, 
    [UserPassword] NVARCHAR(100) NULL, 
    [LockedOut] BIT NULL
)
