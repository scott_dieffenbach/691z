USE [ElectionSeason_DB]
GO

/****** Object:  Table [dbo].[ScreenDefinition]    Script Date: 3/18/2015 7:49:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ScreenDefinition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[screen_desc] [varchar](50) NOT NULL,
	[screen] [varchar](50) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
insert into ScreenDefinition(screen_desc,screen)
values
('Account Creation','AccountCreation.aspx')
GO
insert into ScreenDefinition(screen_desc,screen)
values
('League Creation','LeagueCreation.aspx')
GO


