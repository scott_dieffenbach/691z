﻿USE [master]
GO

/****** Object:  Database [ElectionSeason_DB]    Script Date: 3/11/2015 7:36:07 PM ******/
CREATE DATABASE [ElectionSeason_DB] ON  PRIMARY 
( NAME = N'ElectionSeason_DB', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\ElectionSeason_DB.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ElectionSeason_DB_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\ElectionSeason_DB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [ElectionSeason_DB] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ElectionSeason_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ElectionSeason_DB] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET ARITHABORT OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [ElectionSeason_DB] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [ElectionSeason_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [ElectionSeason_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET  DISABLE_BROKER 
GO

ALTER DATABASE [ElectionSeason_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [ElectionSeason_DB] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [ElectionSeason_DB] SET  MULTI_USER 
GO

ALTER DATABASE [ElectionSeason_DB] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [ElectionSeason_DB] SET DB_CHAINING OFF 
GO

ALTER DATABASE [ElectionSeason_DB] SET  READ_WRITE 
GO


